from appium import webdriver
import yaml
import logging
import logging.config
import os
from os import path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time

log_file_path = path.join(path.dirname(path.abspath(__file__)), "../config/log.conf")
logging.config.fileConfig(log_file_path)
logging = logging.getLogger()


def appium_desired(yaml_address):
    with open(yaml_address,"r",encoding="utf-8") as file:
        data = yaml.load(file)

    desired_caps = {}
    desired_caps['platformName'] = data['platformName']
    desired_caps['platformVersion'] = data['platformVersion']
    desired_caps['deviceName'] = data['deviceName']

    # base_dir = os.path.dirname(os.path.dirname(__file__))
    # app_path = os.path.join(base_dir,'app',data['appname'])

    # desired_caps['app'] = app_path

    desired_caps['appPackage'] = data['appPackage']
    desired_caps['appActivity'] = data['appActivity']
    desired_caps['noReset'] = data['noReset']

    desired_caps['automationName'] = data['automationName']
    desired_caps['unicodeKeyboard'] = data['unicodeKeyboard']
    desired_caps['resetKeyboard'] = data['resetKeyboard']

    logging.info('start app...')
    driver = webdriver.Remote('http://' + str(data['ip']) + ':' + str(data['port']) + '/wd/hub', desired_caps)
   # driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desired_caps)
    driver.implicitly_wait(15)
    return driver


if __name__ == '__main__':
    yaml_address = "../config/desired.yaml"
    driver=appium_desired(yaml_address)
    #e=d.page_source
    # print(b)
    # a="哈哈哈"

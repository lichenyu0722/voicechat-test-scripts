from common.desired_caps import appium_desired
import logging
from time import sleep


class StartEnd():

    def setup(self):
        logging.info('====setup====')
        self.yaml_address = "/Users/fengqi_lichenyu/PycharmProjects/voicechat-test-scripts/config/desired.yaml"
        self.driver = appium_desired(self.yaml_address)

    def teardown(self):
        logging.info('====teardown====')
        sleep(5)
        self.driver.quit()
import os
import time
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.ui import WebDriverWait

from baseView.baseView import BaseView
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
import logging

class Common(BaseView):

    photo_allow = (By.ID,'com.android.packageinstaller:id/permission_allow_button')
    photo_deny = (By.ID,'com.android.packageinstaller:id/permission_deny_button')
    sign_window = (By.ID,'com.test.voice.chat:id/tv_daily_reward')
    close_sign = (By.ID,'com.test.voice.chat:id/icGiftCancel')
    gift_get = (By.ID,'com.test.voice.chat:id/giftGet')
    room_banner = (By.ID,'com.test.voice.chat:id/image')
    event_banner = (By.ID,'com.test.voice.chat:id/ivBannerImg')
    bubble = (By.ID,'com.test.voice.chat:id/tvBubble')

    def get_size(self):
        x = self.driver.get_window_size()['width']
        y = self.driver.get_window_size()['height']
        return x, y

    def swipeMiddleDown(self):
        logging.info("swip Down")
        l = self.get_size()
        a1 = 500/1080
        b1 = 1800/1920
        b2 = 1700/1920
        x1 = int(l[0] * a1)
        y1 = int(l[1] * b1)
        y2 = int(l[1] * b2)
        self.swipe(x1, y1, x1, y2, 1000)

    def swipeLeftDown(self):
        logging.info("swip Down")
        l = self.get_size()
        a1 = 250 / 1080
        b1 = 1800 / 1920
        b2 = 1600 / 1920
        x1 = int(l[0] * a1)
        y1 = int(l[1] * b1)
        y2 = int(l[1] * b2)
        self.swipe(x1, y1, x1, y2, 1000)

    def swipeRightDown(self):
        logging.info("swip Down")
        l = self.get_size()
        a1 = 850 / 1080
        b1 = 1800 / 1920
        b2 = 1500 / 1920
        x1 = int(l[0] * a1)
        y1 = int(l[1] * b2)
        y2 = int(l[1] * b1)
        self.swipe(x1, y1, x1, y2, 1000)

    def get_toast_text(self,text,timeout=5,poll_frequency=0.01):
        '''获取弹出的toast提示'''
        logging.info("====get toast text====")
        toast_element = (By.XPATH, "//*[contains(@text, " + "'" + text + "'" + ")]")
        toast = WebDriverWait(self.driver, timeout, poll_frequency).until(EC.presence_of_element_located(toast_element))
        return toast.text


    def photo_permission_allow(self):
        logging.info("photo permission allow")
        try:
            photo_allow = self.driver.find_element(*self.photo_allow)
        except NoSuchElementException:
            logging.info('can not find the allow button')
            return False
        else:
            logging.info('get photo permission')
            photo_allow.click()
            return True

    def check_sign_window(self):
        '''检测每日签到弹窗'''
        logging.info("check everyday sign window")
        try:
            self.driver.find_element(*self.sign_window)
        except NoSuchElementException:
            logging.info("can not find sign_window")
            return False
        else:
            logging.info("get sign_window")
            return True

    def check_event_banner(self):
        logging.info("check event banner")
        try:
            self.driver.find_element(*self.event_banner)
        except NoSuchElementException:
            logging.info("can not find the event banner")
            return False
        else:
            logging.info("found the event banner")
            return True

    def check_guide_window(self):
        logging.info("check guide window")
        try:
            self.driver.find_element(*self.bubble)
        except NoSuchElementException:
            logging.info("can not find the guide window")
            return False
        else:
            logging.info("found the guide window")
            return True

    def sign_get_gift(self):
        '''签到领取礼物'''
        logging.info("sign to get gift")
        try:
            get_gift = self.driver.find_element(*self.gift_get)
        except NoSuchElementException:
            logging.info("can not find get gift button")
            return False
        else:
            logging.info("click button to get gift")
            get_gift.click()
            try:
                self.driver.find_element(*self.sign_window)
            except NoSuchElementException:
                logging.info("can not find get gift window")
                return False
            else:
                get_gift.click()
                try:
                    self.driver.find_element(*self.sign_window)
                except NoSuchElementException:
                    logging.info("the sign window closed")
                    return True
                else:
                    logging.info("the sign window isn't closed")
                    return False


    def photo_permission_deny(self):
        logging.info("photo permission deny")
        try:
            photo_allow = self.driver.find_element(*self.photo_allow)
        except NoSuchElementException:
            logging.info('can not find the deny button')
            return False
        else:
            logging.info('refuse photo permission')
            photo_allow.click()
            return True

    def get_Time(self):
        '''获取当前时间'''
        self.now = time.strftime("%Y-%m-%d %H_%M_%S")
        return self.now

    def getScreenShot(self,module):
        '''截图操作'''
        time = self.get_Time()
        image_file = os.path.dirname(os.path.dirname(__file__))+'/screenshots/%s_%s.png' %(module,time)

        logging.info('get %s screenshots' %module)
        self.driver.get_screenshot_as_file(image_file)

    def swipeCountryDown(self):
        logging.info("swipe Down")
        l = self.get_size()
        a1 = 500 / 1080
        b1 = 1800 / 1920
        b2 = 500 / 1920
        x1 = int(l[0] * a1)
        y1 = int(l[1] * b1)
        y2 = int(l[1] * b2)
        self.swipe(x1, y1, x1, y2, 1000)

    def swipeDown(self,a1,b1,a2,b2):
        logging.info("swipe down")
        l = self.get_size()
        x1 = a1 / 1080
        x2 = a2 / 1080
        y1 = b1 / 1920
        y2 = b2 / 1920
        self.swipe(int(l[0] * x1),int(l[1] * y1),int(l[0] * x2),int(l[1] * y2),1000)


    def refresh(self):
        logging.info("swipe down to refresh")
        l = self.get_size()
        a1 = 500 / 1080
        b1 = 1800 / 1920
        b2 = 1800 / 1920
        x1 = int(l[0] * a1)
        y1 = int(l[1] * b1)
        y2 = int(l[1] * b2)
        self.swipe(x1, y1, x1, y2, 1000)

    def back(self):
        logging.info("physical back")
        os.system("adb shell input keyevent 4")

    def check_banner(self):
        logging.info("check banner")
        try:
            self.driver.find_element(*self.room_banner)
        except NoSuchElementException:
            logging.info("not have banner")
            return False
        else:
            logging.info("have banner")
            return True



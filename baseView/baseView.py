from selenium.common.exceptions import NoSuchElementException,NoSuchWindowException
import logging,os
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from common.desired_caps import appium_desired
import os
import time




class BaseView(object):
    #继承driver
    def __init__(self, driver):
        self.driver = driver

    #单元素查找
    # def find_element(self, *loc, file_name, element_name):
    #     try:
    #         logging.info("find_element    file-name: {}   element-name: {}".format(file_name, element_name))
    #         find_before_time=time.time()
    #         ele=WebDriverWait(self.driver, timeout=10).until(EC.visibility_of_element_located(*loc))
    #         find_after_time=time.time()
    #         logging.info("find_element use {} s".format(int(find_after_time-find_before_time)))
    #         return ele
    #     except NoSuchElementException:
    #         logging.exception("can't find element   file-name: {}   element-name:  {}".format(file_name, element_name))
    #         raise


    #多元素查找
    def find_elements(self,*loc):
        try:
            logging.info("find_elementssssssssss:{}")
            return self.driver.find_elements(*loc)
        except NoSuchElementException:
            logging.exception("can't find elements:*loc".format(*loc))
            raise


    def find_element_by_accessibility_id(self,*loc):
        try:
            logging.info("find_element_by_accessibility_id:{}".format(*loc))
            return self.driver.find_element_by_accessibility_id(*loc)
        except NoSuchElementException:
            logging.exception("can't find element_by_accessibility")
            raise

    #获取窗口大小
    def get_window_size(self):
        try:
            logging.info("get_window_size")
            return self.driver.get_window_size()
        except NoSuchWindowException:
            logging.exception("get_window_size errorrrrrrrrrrrr")
            raise

    #滑动屏幕
    def swipe(self,start_x,start_y,end_x,end_y,duration):
        try:
            logging.info("start swipe")
            return self.driver.swipe(start_x,start_y,end_x,end_y,duration)
        except NoSuchWindowException:
            logging.exception("swipe errorrrrrrrrrrrrr")
            raise


    #单元素点击
    # def click(self, *loc, file_name, element_name):
    #     try:
    #         logging.info("click element    file-name: {}    element-name: {}".format(file_name, element_name))
    #         before_time=time.time()
    #         self.find_element(*loc, file_name=file_name, element_name=element_name).click()
    #         after_time=time.time()
    #         logging.info("click_element use {} s".format(int(after_time - before_time)))
    #
    #     except NoSuchWindowException:
    #         logging.exception("click errorrrrrrrrrr   file-name: {}    element-name: {}".format(file_name, element_name))





if __name__ == '__main__':
    name=os.path.abspath(__file__).split("voicechat-test-scripts")[1]
    yaml_address = "../config/desired.yaml"
    driver = appium_desired(yaml_address)
    # ps = driver.page_source
    me = (By.ID, 'com.test.voice.chat:id/flMe')
    me_header = (By.ID, 'com.test.voice.chat:id/me_header_image')
    # BaseView(driver).click(me, file_name=name, element_name="me")
    # BaseView(driver).click(me_header, file_name=name, element_name="me_header")
    before_time=time.time()
    # a=driver.find_element(*me)
    a=WebDriverWait(driver, timeout=10).until(EC.visibility_of_element_located(me))
    a.click()
    after_time=time.time()
    print(int(after_time-before_time))
    before_time1=time.time()
    # b=driver.find_element(*me_header)
    b=WebDriverWait(driver, timeout=10).until(EC.visibility_of_element_located(me_header))
    b.click()
    after_time1=time.time()
    print(int(after_time1-before_time1))


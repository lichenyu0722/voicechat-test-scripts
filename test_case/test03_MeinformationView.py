from common.myunit import StartEnd
from businessView.meFunction.meInformation.MeInformationView import MeInformationView
import pytest
import logging
import allure

class Test_MeinformationView(StartEnd):

    @allure.feature("test_back_to_MePage")
    @pytest.mark.flaky(reruns=3,reruns_delay=2)
    def test_back_to_MePage(self):
        logging.info("====back to MePage====")
        l = MeInformationView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.back_to_mePage() == True

    @allure.feature("test_go_MeEditView")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_MeEditView(self):
        logging.info("====go to MeEditView====")
        l = MeInformationView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True

    @allure.feature("test_get_Me_username")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_get_Me_username(self):
        logging.info("====get Me username====")
        l = MeInformationView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        l.get_Me_username()

    @allure.feature("test_get_Me_bio")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_get_Me_bio(self):
        logging.info("====get Me bio====")
        l = MeInformationView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        l.get_Me_bio()

if __name__ == '__main__':
    pytest.main(['-s','q','test03_MeinformationView.py'])
from common.myunit import StartEnd
from businessView.meFunction.meStore.StorePageView import StorePageView
import pytest
import logging
import allure

class Test_StorePage(StartEnd):

    @allure.feature("test_go_store_page")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_store_page(self):
        logging.info("test_go_store_page")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.check_store_page() == True

    @allure.feature("test_switch_mine_page")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_mine_page(self):
        logging.info("test_switch_mine_page")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.check_mine_page() == True

    @allure.feature("test_switch_store_page")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_store_page(self):
        logging.info("test_switch_store_page")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.switch_store() == True
        assert l.check_store_page() == True

    @allure.feature("test_switch_store_frames")
    @pytest.mark.flaky(reruns=3,reruns_delay=2)
    def test_switch_store_frames(self):
        logging.info("test_switch_store_frames")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Frames") == True

    @allure.feature("test_switch_store_mounts")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_store_mounts(self):
        logging.info("test_switch_store_mounts")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Mounts") == True

    @allure.feature("test_switch_store_relation")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_store_relation(self):
        logging.info("test_switch_store_relation")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Relation") == True

    @allure.feature("test_switch_mine_frames")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_mine_frames(self):
        logging.info("test_switch_mine_frames")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.switch_mine_tab(tab="Frames") == True

    @allure.feature("test_switch_mine_mounts")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_mine_mounts(self):
        logging.info("test_switch_mine_mounts")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.switch_mine_tab(tab="Mounts") == True

    @allure.feature("test_switch_mine_chat_box")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_mine_chat_box(self):
        logging.info("test_switch_mine_chat_box")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.switch_mine_tab(tab="Chat Box") == True

    @allure.feature("test_switch_mine_room_frame")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_switch_mine_room_frame(self):
        logging.info("test_switch_mine_room_frame")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_mine() == True
        assert l.switch_mine_tab(tab="Room Frame") == True

    @allure.feature("test_relation_rules_page")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_relation_rules_page(self):
        logging.info("test_relation_rules_page")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.go_relationship_rules_page() == True

    @allure.feature("test_buy_frames_goods")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_frames_goods(self):
        logging.info("test_buy_frames_goods")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Frames")
        assert l.buy_goods(1) == True

    @allure.feature("test_buy_mounts_goods")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_mounts_goods(self):
        logging.info("test_buy_mounts_goods")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Mounts")
        assert l.buy_goods(1) == True

    @allure.feature("test_buy_relation_goods")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_relation_goods(self):
        logging.info("test_buy_relation_goods")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.switch_store_tab(tab="Relation")
        assert l.buy_goods(1) == True

    @allure.feature("test_buy_success_twice_window_enable")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_success_twice_window_enable(self):
        logging.info("test_buy_success_twice_window_enable")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.buy_goods(1) == True
        assert l.buy_success_twice_window(option="enable") == True
        assert l.check_mine_page() == True

    @allure.feature("test_buy_success_twice_window_later")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_success_twice_window_later(self):
        logging.info("test_buy_success_twice_window_later")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.buy_goods(1) == True
        assert l.buy_success_twice_window(option="later") == True
        assert l.check_store_page() == True

    '''因有可能出现异常情况导致无法捕捉到Android提示toast，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case maybe failure")
    @allure.feature("test_send_goods")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_send_goods(self):
        logging.info("test_send_goods")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.switch_store() == True
        assert l.send_goods(1,option="send") == True

    @allure.feature("test_try_goods")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_try_goods(self):
        logging.info("test_try_goods")
        l = StorePageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Store() == True
        assert l.try_goods(1) == True


if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test11_StorePage.py'])
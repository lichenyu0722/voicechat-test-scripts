from common.myunit import StartEnd
from businessView.meFunction.meLevel.LevelPageView import LevelPageView
import pytest
import allure
import logging

class Test_UserLevel(StartEnd):
    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''

    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_go_UserLevelPage")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_UserLevelPage(self):
        logging.info("test_go_UserLevelPage")
        l = LevelPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Level() ==True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_open_LevelInfo")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_open_LevelInfo(self):
        logging.info("test_open_LevelInfo")
        l = LevelPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Level() == True
        assert l.open_level_info() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_close_LevelInfo")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_close_LevelInfo(self):
        logging.info("test_close_LevelInfo")
        l = LevelPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Level() == True
        assert l.open_level_info() == True
        assert l.close_level_info() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_back_MePage")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_back_MePage(self):
        logging.info("test_back_MePage")
        l = LevelPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Level() == True
        assert l.back_to_MePage() == True

if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test06_UserLevel.py'])

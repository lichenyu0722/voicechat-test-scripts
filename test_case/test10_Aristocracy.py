from common.myunit import StartEnd
from businessView.meFunction.meAristocracy.AristocracyPageView import AristocracyPageView
import pytest
import logging
import allure

class Test_Aristocracy(StartEnd):

    @allure.feature("test_go_aristocracy_page")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_aristocracy_page(self):
        logging.info("test_go_aristocracy_page")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True

    @allure.feature("test_select_viscount_aristocracy")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_select_viscount_aristocracy(self):
        logging.info("test_select_viscount_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("viscount") == True

    @allure.feature("test_select_count_aristocracy")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_select_count_aristocracy(self):
        logging.info("test_select_count_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("count") == True

    @allure.feature("test_select_marquis_aristocracy")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_select_marquis_aristocracy(self):
        logging.info("test_select_marquis_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("marquis") == True

    @allure.feature("test_select_duke_aristocracy")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_select_duke_aristocracy(self):
        logging.info("test_select_duke_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("duke") == True

    '''因有可能出现拥有高等级贵族购买低等级贵族不成功情况，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_buy_viscount_aristocracy")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_viscount_aristocracy(self):
        logging.info("test_buy_viscount_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("viscount") == True
        assert l.buy_aristocracy() == True
        assert l.check_for_aristocracy("viscount") == True

    '''因有可能出现拥有高等级贵族购买低等级贵族不成功情况，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_buy_count_aristocracy")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_count_aristocracy(self):
        logging.info("test_buy_count_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("count") == True
        assert l.buy_aristocracy() == True
        assert l.check_for_aristocracy("count") == True

    '''因有可能出现拥有高等级贵族购买低等级贵族不成功情况，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_buy_marquis_aristocracy")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_marquis_aristocracy(self):
        logging.info("test_buy_marquis_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("marquis") == True
        assert l.buy_aristocracy() == True
        assert l.check_for_aristocracy("marquis") == True

    '''因有可能出现拥有高等级贵族购买低等级贵族不成功情况，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_buy_duke_aristocracy")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_buy_duke_aristocracy(self):
        logging.info("test_buy_duke_aristocracy")
        l = AristocracyPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Aristocracy() == True
        assert l.select_aristocracy("duke") == True
        assert l.buy_aristocracy() == True
        assert l.check_for_aristocracy("duke") == True


if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test10_Aristocracy.py'])

from common.myunit import StartEnd
from businessView.meFunction.meWallet.DIamondPageView import DiamondPageView
import pytest
import logging
import allure

class Test_DiamondChangeCoins(StartEnd):

    @allure.feature("test_D10_to_C99")
    @pytest.mark.flaky(reruns =3,reruns_delay=2)
    def test_D10_to_C99(self):
        logging.info("test_D10_to_C99")
        D10_to_C99 = 0 #10钻石兑换99金币
        l = DiamondPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Diamonds() == True
        assert l.go_Coins_Exchange() == True
        before_coins = l.get_exchange_coins_balance()
        before_diamonds = l.get_exchange_diamond_balance()
        exchange_coins = l.get_exchange_coins_number(D10_to_C99)
        exchange_diamonds = l.get_exchange_diamonds_number(D10_to_C99)
        assert l.diamonds_to_coins(D10_to_C99) == True
        assert l.twice_window(True) == True
        after_coins = l.get_exchange_coins_balance()
        after_diamonds = l.get_exchange_diamond_balance()
        assert l.check_exchange_result(before_diamonds=before_diamonds, before_coins=before_coins,exchange_coins=exchange_coins,
                                       exchange_diamonds=exchange_diamonds, after_coins=after_coins,after_diamonds=after_diamonds) == True
        print("before coins: " + str(before_coins))
        print("before diamonds: " + str(round(before_diamonds, 2)))
        print("exchange coins: " + str(exchange_coins))
        print("exchange diamonds: " + str(round(exchange_diamonds, 2)))
        print("after coins: " + str(after_coins))
        print("after diamonds: " + str(round(after_diamonds, 2)))

    @allure.feature("test_D20_to_C198")
    @pytest.mark.flaky(reruns=3,reruns_delay=2)
    def test_D20_to_C198(self):
        logging.info("test_D20_to_C198")
        D20_to_C198 = 1  # 20钻石兑换198金币
        l = DiamondPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Diamonds() == True
        assert l.go_Coins_Exchange() == True
        before_coins = l.get_exchange_coins_balance()
        before_diamonds = l.get_exchange_diamond_balance()
        exchange_coins = l.get_exchange_coins_number(D20_to_C198)
        exchange_diamonds = l.get_exchange_diamonds_number(D20_to_C198)
        assert l.diamonds_to_coins(D20_to_C198) == True
        assert l.twice_window(True) == True
        after_coins = l.get_exchange_coins_balance()
        after_diamonds = l.get_exchange_diamond_balance()
        assert l.check_exchange_result(before_diamonds=before_diamonds, before_coins=before_coins,
                                       exchange_coins=exchange_coins,
                                       exchange_diamonds=exchange_diamonds, after_coins=after_coins,
                                       after_diamonds=after_diamonds) == True
        print("before coins: " + str(before_coins))
        print("before diamonds: " + str(round(before_diamonds, 2)))
        print("exchange coins: " + str(exchange_coins))
        print("exchange diamonds: " + str(round(exchange_diamonds, 2)))
        print("after coins: " + str(after_coins))
        print("after diamonds: " + str(round(after_diamonds, 2)))

    @allure.feature("test_D30_to_C297")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_D30_to_C297(self):
        logging.info("test_D30_to_C297")
        D30_to_C297 = 2  # 30钻石兑换297金币
        l = DiamondPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Diamonds() == True
        assert l.go_Coins_Exchange() == True
        before_coins = l.get_exchange_coins_balance()
        before_diamonds = l.get_exchange_diamond_balance()
        exchange_coins = l.get_exchange_coins_number(D30_to_C297)
        exchange_diamonds = l.get_exchange_diamonds_number(D30_to_C297)
        assert l.diamonds_to_coins(D30_to_C297) == True
        assert l.twice_window(True) == True
        after_coins = l.get_exchange_coins_balance()
        after_diamonds = l.get_exchange_diamond_balance()
        assert l.check_exchange_result(before_diamonds=before_diamonds, before_coins=before_coins,
                                       exchange_coins=exchange_coins,
                                       exchange_diamonds=exchange_diamonds, after_coins=after_coins,
                                       after_diamonds=after_diamonds) == True
        print("before coins: " + str(before_coins))
        print("before diamonds: " + str(round(before_diamonds, 2)))
        print("exchange coins: " + str(exchange_coins))
        print("exchange diamonds: " + str(round(exchange_diamonds, 2)))
        print("after coins: " + str(after_coins))
        print("after diamonds: " + str(round(after_diamonds, 2)))

    @allure.feature("test_D50_to_C495")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_D50_to_C495(self):
        logging.info("test_D50_to_C495")
        D50_to_C495 = 3  # 50钻石兑换495金币
        l = DiamondPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Diamonds() == True
        assert l.go_Coins_Exchange() == True
        before_coins = l.get_exchange_coins_balance()
        before_diamonds = l.get_exchange_diamond_balance()
        exchange_coins = l.get_exchange_coins_number(D50_to_C495)
        exchange_diamonds = l.get_exchange_diamonds_number(D50_to_C495)
        assert l.diamonds_to_coins(D50_to_C495) == True
        assert l.twice_window(True) == True
        after_coins = l.get_exchange_coins_balance()
        after_diamonds = l.get_exchange_diamond_balance()
        assert l.check_exchange_result(before_diamonds=before_diamonds, before_coins=before_coins,
                                       exchange_coins=exchange_coins,
                                       exchange_diamonds=exchange_diamonds, after_coins=after_coins,
                                       after_diamonds=after_diamonds) == True
        print("before coins: " + str(before_coins))
        print("before diamonds: " + str(round(before_diamonds, 2)))
        print("exchange coins: " + str(exchange_coins))
        print("exchange diamonds: " + str(round(exchange_diamonds, 2)))
        print("after coins: " + str(after_coins))
        print("after diamonds: " + str(round(after_diamonds, 2)))

    @allure.feature("test_D100_to_C990")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_D100_to_C990(self):
        logging.info("test_D100_to_C990")
        D100_to_C990 = 4  # 100钻石兑换990金
        l = DiamondPageView(self.driver)
        assert l.go_Me() == True
        assert l.go_Diamonds() == True
        assert l.go_Coins_Exchange() == True
        before_coins = l.get_exchange_coins_balance()
        before_diamonds = l.get_exchange_diamond_balance()
        exchange_coins = l.get_exchange_coins_number(D100_to_C990)
        exchange_diamonds = l.get_exchange_diamonds_number(D100_to_C990)
        assert l.diamonds_to_coins(D100_to_C990) == True
        assert l.twice_window(True) == True
        after_coins = l.get_exchange_coins_balance()
        after_diamonds = l.get_exchange_diamond_balance()
        assert l.check_exchange_result(before_diamonds=before_diamonds, before_coins=before_coins,
                                       exchange_coins=exchange_coins,
                                       exchange_diamonds=exchange_diamonds, after_coins=after_coins,
                                       after_diamonds=after_diamonds) == True
        print("before coins: " + str(before_coins))
        print("before diamonds: " + str(round(before_diamonds, 2)))
        print("exchange coins: " + str(exchange_coins))
        print("exchange diamonds: " + str(round(exchange_diamonds, 2)))
        print("after coins: " + str(after_coins))
        print("after diamonds: " + str(round(after_diamonds, 2)))

if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test05_DiamondChangeCoins.py'])
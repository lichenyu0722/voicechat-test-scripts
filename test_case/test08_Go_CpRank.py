from common.myunit import StartEnd
from businessView.homeFunction.HomeHotView import HomeHotView
import pytest
import logging
import allure

class Test_go_CpRank(StartEnd):

    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_go_CP_rank")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_cp_rank(self):
        logging.info("test_go_cp_rank")
        l = HomeHotView(self.driver)
        assert l.go_hot() == True
        assert l.go_CP_ranking() == True

if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test08_Go_CpRank.py'])
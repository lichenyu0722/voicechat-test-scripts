from common.myunit import StartEnd
from businessView.homeFunction.HomeHotView import HomeHotView
import pytest
import logging
import allure

class Test_go_GlobalRank(StartEnd):
    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''

    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_go_CP_rank")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_CP_rank(self):
        logging.info("test_go_CP_rank")
        l = HomeHotView(self.driver)
        assert l.go_hot() == True
        assert l.go_CP_ranking() == True

    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_go_Activity")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_Activity(self):
        logging.info("test_go_Activity")
        l = HomeHotView(self.driver)
        assert l.go_hot() == True
        assert l.go_Activity() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    @allure.feature("test_go_Rooms_rank")
    # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_Rooms_rank(self):
        logging.info("test_go_Rooms_rank")
        l = HomeHotView(self.driver)
        assert l.go_hot() == True
        assert l.go_ranking() == True
        assert l.check_rooms_rank() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    # @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    # @allure.feature("test_go_Wealth_rank")
    # # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    # def test_go_Wealth_rank(self):
    #     logging.info("test_go_Wealth_rank")
    #     l = HomeHotView(self.driver)
    #     assert l.go_hot() == True
    #     assert l.go_ranking(rankName="Wealth") == True
    #     assert l.check_wealth_rank() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    # @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    # @allure.feature("test_go_Charm_rank")
    # # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    # def test_go_Charm_rank(self):
    #     logging.info("test_go_Charm_rank")
    #     l = HomeHotView(self.driver)
    #     assert l.go_hot() == True
    #     assert l.go_ranking(rankName="Charm") == True
    #     assert l.check_charm_rank() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    # @pytest.mark.xfail(condition=lambda: True, reason="this case is expecting failure")
    # @allure.feature("test_go_Gift_rank")
    # # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    # def test_go_Gift_rank(self):
    #     logging.info("test_go_Gift_rank")
    #     l = HomeHotView(self.driver)
    #     assert l.go_hot() == True
    #     assert l.go_ranking(rankName="Gift") == True
    #     assert l.check_gift_rank() == True

if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test07_Go_GlobalRank.py'])
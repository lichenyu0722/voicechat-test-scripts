from common.myunit import StartEnd
import pytest,time,os
import logging
import allure
from businessView.loginAndRegisterFunction.LoginView import LoginView

class Test_LoginView(StartEnd):

    @allure.feature("test_10_numbers_login")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_10_numbers_login(self):
        logging.info("10 numbers login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="1531111111") == False

    @allure.feature("test_0_numbers_login")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_0_numbers_login(self):
        logging.info("0 numbers login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="  ") == False

    @allure.feature("test_12_numbers_login")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_12_numbers_login(self):
        logging.info("12 numbers login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="153111111171") == False

    @allure.feature("test_0_verification")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_0_verification(self):
        logging.info("0 verification code login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="15511111118") == True
        assert l.verification_code_input("      ") == True
        assert l.check_login() == False

    @allure.feature("test_3_verification")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_3_verification(self):
        logging.info("3 verification code login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="15511111118") == True
        assert l.verification_code_input("123   ") == True
        assert l.check_login() == False

    @allure.feature("test_wrong_verification")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_wrong_verification(self):
        logging.info("3 verification code login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="15511111118") == True
        assert l.verification_code_input("654321") == True
        assert l.check_login() == False

    @allure.feature("test_normal_login")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_normal_login(self):
        logging.info("normally login")
        l = LoginView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="15511111118") == True
        assert l.verification_code_input("123456") == True
        time.sleep(2)
        if l.check_event_banner() == True:
            os.system("adb shell input keyevent 4")
        else:
            pass
        if l.check_sign_window() == True:
            os.system("adb shell input keyevent 4")
        else:
            pass
        assert l.check_login() == True

if __name__ == '__main__':
    pytest.main(['-s','q','test02_LoginView.py'])
from common.myunit import StartEnd
from businessView.meFunction.meInformation.MeEditView import MeEditView
import pytest
import logging
import random,string
import allure

class Test_MeEditView(StartEnd):

    @allure.feature("test_username_edit")
    @pytest.mark.flaky(reruns=3,reruns_delay=2)
    def test_username_edit(self):
        logging.info("===edit the username===")
        l = MeEditView(self.driver)
        ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.username_edit(ran_name) == True
        assert l.done_click() ==True
        assert (l.get_Me_username()) == ran_name

    @allure.feature("test_username_nochange_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_username_nochange_edit(self):
        logging.info("===edit the username===")
        l = MeEditView(self.driver)
        # ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        name = l.get_Me_username()
        assert l.go_Me_information_edit() == True
        assert l.username_edit(name)
        assert l.done_click() == False

    @allure.feature("test_birthday_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_birthday_edit(self):
        logging.info("===edit birthday===")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.birthday_edit() == True
        l.birthday_select()
        assert l.birthday_saved() == True
        assert l.done_click() == True

    @allure.feature("test_birthday_nochange_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_birthday_nochange_edit(self):
        logging.info("===edit birthday===")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.birthday_edit() == True
        assert l.birthday_saved() == True
        assert l.done_click() == False

    @allure.feature("test_country_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_country_edit(self):
        logging.info("===edit country===")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.country_edit() == True
        assert l.done_click() == True

    @allure.feature("test_header_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_header_edit(self):
        logging.info("===edit header===")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.header_edit() == True
        assert l.done_click() == True

    @allure.feature("test_bio_edit")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_bio_edit(self):
        logging.info("===edit bio===")
        l = MeEditView(self.driver)
        ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 20))
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        assert l.bio_edit(ran_str) == True
        assert l.done_click() == True
        assert l.get_Me_bio() == ran_str

    @allure.feature("test_bio_bank_done")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_bio_bank_done(self):
        logging.info("====edit bio====")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        logging.info("输入全部为空格")
        assert l.bio_edit("     ") == False

    @allure.feature("test_username_bank_done")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_username_bank_done(self):
        logging.info("===edit the username===")
        l = MeEditView(self.driver)
        assert l.go_Me() == True
        assert l.go_Me_information() == True
        assert l.go_Me_information_edit() == True
        logging.info("输入全部为空格")
        assert l.username_edit("    ") == True
        assert l.done_click() == False


if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test04_MeEditView.py'])
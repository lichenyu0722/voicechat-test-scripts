from common.myunit import StartEnd
from businessView.homeFunction.HomeExploreView import HomeExploreView
import pytest
import logging
import allure

class Test_ExplorePage(StartEnd):

    @allure.feature("test_go_more_country")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_more_country(self):
        logging.info("test_go_more_country")
        l = HomeExploreView(self.driver)
        assert l.go_explore() == True
        assert l.go_more_country() == True

    '''因页面不稳定，该测试用例执行条件可能存在失败，若失败请手动回归测试'''
    @pytest.mark.xfail(condition=lambda: True, reason="maybe the more_gift_rank disappear")
    @allure.feature("test_go_more_gift_rank")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_go_more_gift_rank(self):
        logging.info("test_go_more_gift_rank")
        l = HomeExploreView(self.driver)
        assert l.go_explore() == True
        assert l.go_more_GiftRank() == True
        assert l.check_gift_rank() == True

if __name__ == '__main__':
    pytest.main(['-s', 'q', 'test09_ExplorePage.py'])
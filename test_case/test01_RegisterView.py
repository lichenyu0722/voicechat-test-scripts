from common.myunit import StartEnd
import pytest,time,random,string
import logging
import allure
from businessView.loginAndRegisterFunction.RegisterView import RegisterView


class Test_RegisterView(StartEnd):

    @allure.feature("test_no_username_register")
    @pytest.mark.flaky(reruns=3,reruns_delay=2)
    def test_no_username_register(self):
        logging.info("no username register")
        l = RegisterView(self.driver)
        l.check_sign_window()
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="13355555501") == True
        assert l.verification_code_input("123456") == True
        time.sleep(2)
        assert l.first_header_edit() == True
        assert l.username_clear() == True
        assert l.select_gender(gender="Male") == True
        assert l.select_age() == True
        assert l.confirm_click() == True
        assert l.check_login() == False

    # '''因注册页面改变，故忽略此条用例，跳过此条失败的测试用例'''
    # @pytest.mark.xfail(condition=lambda:True,reason="this case is expecting failure")
    # @allure.feature("test_no_age_register")
    # # @pytest.mark.flaky(reruns=3, reruns_delay=2)
    # def test_no_age_register(self):
    #     logging.info("no age register")
    #     l = RegisterView(self.driver)
    #     assert l.go_Phone_login() == True
    #     assert l.phone_country_select("China") == True
    #     assert l.phone_number_input(number="18611111117") == True
    #     assert l.verification_code_input("123456") == True
    #     time.sleep(2)
    #     assert l.first_header_edit() == True
    #     ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
    #     assert l.username_input(ran_name) == True
    #     # assert l.select_gender(gender="female") == True
    #     assert l.confirm_click() == True
    #     assert l.check_login() == False

    @allure.feature("test_no_gender_register")
    @pytest.mark.flaky(reruns=3, reruns_delay=2)
    def test_no_gender_register(self):
        logging.info("no gender register")
        l = RegisterView(self.driver)
        assert l.go_Phone_login() == True
        assert l.phone_country_select("China") == True
        assert l.phone_number_input(number="13355555501") == True
        assert l.verification_code_input("123456") == True
        time.sleep(2)
        assert l.first_header_edit() == True
        assert l.username_clear() == True
        ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
        assert l.username_input(ran_name) == True
        assert l.select_age() == True
        assert l.confirm_click() == True
        assert l.check_login() == False

if __name__ == '__main__':
    pytest.main(['-s','q','test01_RegisterView.py'])
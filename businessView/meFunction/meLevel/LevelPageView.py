from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.MePageVIew import MePageView
import logging


class LevelPageView(MePageView):

    back_button = (By.XPATH, '//android.widget.ImageButton')
    level_info = (By.ID,'com.test.voice.chat:id/action_level')
    title = (By.ID,'com.test.voice.chat:id/tv_title')
    info_close = (By.ID,'com.test.voice.chat:id/btn_dismiss')

    def back_to_MePage(self):
        '''回到我的界面'''
        logging.info("=====back to MePage=====")
        try:
            back = self.driver.find_element(*self.back_button)
        except NoSuchElementException:
            logging.info('can not find the back button')
            return False
        else:
            back.click()
            try:
                self.driver.find_element(*self.me_header)
            except NoSuchElementException:
                logging.info('back to MePage Failed')
                return False
            else:
                logging.info('back to MePage Successed')
                return True

    def open_level_info(self):
        '''打开等级介绍'''
        logging.info("====open level info====")
        try:
            level_info = self.driver.find_element(*self.level_info)
        except NoSuchElementException:
            logging.info("can not find the level info")
            return False
        else:
            logging.info("open level info")
            level_info.click()
            try:
                title = self.driver.find_element(*self.title)
            except NoSuchElementException:
                logging.info("open level info failed")
                return False
            else:
                if title.get_attribute("text") == "Level Label Rewards":
                    logging.info("open level info success")
                    return True
                else:
                    logging.info("open level info failed")
                    return False

    def close_level_info(self):
        '''关闭等级介绍'''
        logging.info("====close level info=====")
        try:
            close_btn = self.driver.find_element(*self.info_close)
        except NoSuchElementException:
            logging.info("can not find the close button")
            return False
        else:
            logging.info("close level info")
            close_btn.click()
            try:
                self.driver.find_element(*self.level)
            except NoSuchElementException:
                logging.info("close level info failed")
                return False
            else:
                logging.info("close level info success")
                return True



if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = LevelPageView(appium_desired(yaml_address))
    l.go_Me()
    l.go_Level()
    l.back_to_MePage()
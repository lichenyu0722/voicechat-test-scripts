from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from businessView.meFunction.MePageVIew import MePageView
from businessView.homeFunction.HomeHotView import HomeHotView
from selenium.webdriver.common.by import By
import logging

class CoinTask(MePageView,HomeHotView):

    back = (By.ID,'com.test.voice.chat:id/ivBack')

    def switch_task_tab(self,task):
        if self.switch_coin_task(task) == True:
            logging.info("switch task success")
            return True
        else:
            logging.info("switch task failed")
            return False

    def back_to_me(self):
        '''返回到Me页面'''
        # self.go_Me_information()
        logging.info("====back to mePage====")
        try:
            back = self.driver.find_element(*self.back)
        except NoSuchElementException:
            logging.info('can not find the back button')
            return False
        else:
            back.click()
            try:
                self.driver.find_element(*self.me_header)
            except NoSuchElementException:
                logging.info('back to MePage Failed')
                return False
            else:
                logging.info('back to MePage Successed')
                return True

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = CoinTask(appium_desired(yaml_address))
    l.go_Me()
    l.go_coin_task()
    l.switch_task_tab("Improvement task")
    l.back_to_me()



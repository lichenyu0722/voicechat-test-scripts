from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.meWallet.WalletPageView import WalletPageView
import logging,random,time


class StorePageView(WalletPageView):

    Store = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]/'
                      'android.widget.LinearLayout[1]/android.widget.LinearLayout[1]')
    Mine = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]'
                     '/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]')
    frames = (By.ID,'com.test.voice.chat:id/tab_item_1')
    mounts = (By.ID,'com.test.voice.chat:id/tab_item_2')
    chatbox = (By.ID,'com.test.voice.chat:id/tab_item_3')
    roomframes = (By.ID,'com.test.voice.chat:id/tab_item_4')
    tabdict = {"Frames": frames,"Mounts": mounts,"Chat Box": chatbox,"Room Frame": roomframes}
    relationship_rules = (By.ID,'com.test.voice.chat:id/btn_right')
    title = (By.ID,'com.test.voice.chat:id/title')
    buys = (By.ID,'com.test.voice.chat:id/tvBuy')
    buySuccessContent = (By.ID,'com.test.voice.chat:id/tvBuySuccessContent')
    send = (By.ID,'com.test.voice.chat:id/tvSend')
    cancel = (By.ID,'com.test.voice.chat:id/tvCancel')
    ivtry = (By.ID,'com.test.voice.chat:id/ivTry')
    dialog_positive = (By.ID,'com.test.voice.chat:id/dialog_positive')
    dialog_nagative = (By.ID,'com.test.voice.chat:id/dialog_nagative')
    dialog_message = (By.ID,'com.test.voice.chat:id/dialog_message')
    ivclose = (By.ID,'com.test.voice.chat:id/ivClose')

    def check_store_page(self):
        '''检查是否在store页面'''
        logging.info("=====check store page====")
        try:
            store = self.find_element(*self.Store)
        except NoSuchElementException:
            logging.info("can not find the Store")
            return False
        else:
            if store.get_attribute("clickable") == "true":
                logging.info('you are not in store page')
                return False
            elif store.get_attribute("clickable") == "false":
                logging.info("now you are in store page")
                return True

    def check_mine_page(self):
        '''检查是否在mine页面'''
        logging.info("=====check mine page=====")
        try:
            mine = self.find_element(*self.Mine)
        except NoSuchElementException:
            logging.info("can not find the Mine")
            return False
        else:
            if mine.get_attribute("clickable") == "true":
                logging.info("you are not in mine page")
                return False
            elif mine.get_attribute("clickable") == "false":
                logging.info("now you are in mine page")
                return True

    def switch_store(self):
        '''切换至商场页'''
        logging.info("=====switch to store page====")
        if self.check_store_page() == False:
            logging.info("switch to store page")
            self.find_element(*self.Store).click()
            if self.check_store_page() == True:
                logging.info("now you are in store page")
                return True
        elif self.check_store_page() == True:
            logging.info("you are already in store page")
            return True
        else:
            logging.info("something wrong")
            return False

    def switch_mine(self):
        '''切换至我的页面'''
        logging.info("=====switch to mine page=====")
        if self.check_mine_page() == False:
            logging.info("switch to mine page")
            self.find_element(*self.Mine).click()
            if self.check_mine_page() == True:
                logging.info("now you are in mine page")
                return True
        elif self.check_mine_page() == True:
            logging.info("you are already in mine page")
            return True
        else:
            logging.info("something wrong")
            return False

    def switch_store_tab(self,tab):
        '''切换至Store-tab页面
            tab:  Frames/Mounts/Relation
        '''
        logging.info("=====switch to Store-%s====="%(tab))
        try:
            tabs = self.driver.find_element_by_accessibility_id(tab)
        except NoSuchElementException:
            logging.info("can not find Store-%s"%(tab))
            return False
        else:
            logging.info("click to go Store-%s Page"%(tab))
            tabs.click()
            if tabs.get_attribute("clickable") == "false":
                logging.info("now you are in Store-%s Page"%(tab))
                return True
            else:
                logging.info("you are not in Store-%s Page"%(tab))
                return False

    def switch_mine_tab(self,tab):
        '''
        切换至Mine-tab页面
            tab： Frames/Mounts/Chat Box/Room Frame
        '''
        i = 0
        tabs = list(self.tabdict)
        print(tabs)
        while i<len(tabs):
            if tab == tabs[i]:
                logging.info("=====switch to Mine-%s====="%(tabs[i]))
                try:
                    tabtab = self.driver.find_element(*self.tabdict[tabs[i]])
                except NoSuchElementException:
                    logging.info("can not find the Mine-%s"%(tabs[i]))
                    return False
                else:
                    logging.info("click to go Mine-%s Page"%(tabs[i]))
                    tabtab.click()
                    return True
            i += 1

    def go_relationship_rules_page(self):
        '''进入relationship rules页面'''
        logging.info("=====go relationship rules page=====")
        try:
            relationship_rules = self.driver.find_element(*self.relationship_rules)
        except NoSuchElementException:
            logging.info("can not find the relationship rules")
            return False
        else:
            logging.info("click to go relationship rules page")
            relationship_rules.click()
            try:
                title = self.driver.find_element(*self.title)
            except NoSuchElementException:
                logging.info("you are not in relationship rules page")
                return False
            else:
                if title.get_attribute("text") == "Relationship rules":
                    logging.info("now you are in relationship rules page")
                    return True
                else:
                    logging.info("you are not in relationship rules page")
                    return False

    def buy_goods(self,i):
        '''购买商品'''
        logging.info("=====buy goods=====")
        try:
            buy = self.driver.find_elements(*self.buys)[i]
        except NoSuchElementException:
            logging.info("can not find the buy button")
            return False
        else:
            logging.info("click to buy")
            buy.click()
            try:
                sure = self.driver.find_element(*self.dialog_positive)
            except NoSuchElementException:
                logging.info("can not find the twice window")
                return False
            else:
                logging.info("sure to buy")
                sure.click()
                try:
                    self.driver.find_element(*self.buySuccessContent)
                except NoSuchElementException:
                    logging.info("maybe bought it failed")
                    return False
                else:
                    logging.info("maybe success")
                    return True

    def buy_success_twice_window(self,option):
        '''购买成功二次弹窗页面操作'''
        logging.info("buy success twice window")
        if option == "enable":
            self.driver.find_element(*self.dialog_positive).click()
            logging.info("click to go Mine Page")
            return True
        elif option == "later":
            self.driver.find_element(*self.dialog_nagative).click()
            logging.info("still Store Page")
            return True
        else:
            logging.info("something wrong")
            return False

    def send_goods(self,i,option):
        '''赠送商品'''
        logging.info("=====send goods=====")
        try:
            send = self.driver.find_elements(*self.send)[i]
        except NoSuchElementException:
            logging.info("can not find the send button")
            return False
        else:
            logging.info("click to send")
            send.click()
            j = random.randint(0, 6)
            try:
                send = self.driver.find_elements(*self.send)[0]
            except NoSuchElementException:
                logging.info("select send guy")
                return False
            else:
                send.click()
                logging.info("twice send window")
                if option == "send":
                    time.sleep(1)
                    self.driver.find_element(*self.send).click()
                    try:
                        self.driver.find_element(*self.dialog_message)
                    except NoSuchElementException:
                        logging.info("maybe success")
                        return True
                    else:
                        logging.info("maybe need recharge")
                        if self.driver.find_element(*self.dialog_message).get_attribute("text") == "Not enough coins，want to recharge?":
                            logging.info("Not enough coins，want to recharge?")
                            self.driver.find_element(*self.dialog_positive).click()
                            i = 0
                            coin_before = self.get_coin_balance()
                            self.select_recharge_way(way="google")
                            coin_recharge = self.get_recharge(i)
                            self.click_Coin_toBuy(i)
                            time.sleep(10)
                            coin_after = self.get_coin_balance()
                            self.check_recharge(coin_before, coin_recharge, coin_after)
                            logging.info("coin_berfore: " + str(coin_before))
                            logging.info("recharge coin: " + str(coin_recharge))
                            logging.info("coin_after: " + str(coin_after))
                            self.back()
                            self.driver.find_element(*self.send).click()
                            if self.get_toast_text("Success") == "Success":
                                logging.info("send success")
                                return True
                            else:
                                logging.info("something wrong")
                                return False
                elif option == "cancel":
                    time.sleep(1)
                    self.driver.find_element(*self.cancel).click()
                    try:
                        text = self.driver.find_element(*self.title).get_attribute("text")
                    except NoSuchElementException:
                        logging.info("you are not in send friends page")
                        return False
                    else:
                        if "Friends" in text:
                            logging.info("you are back to send frineds page")
                            return True
                        else:
                            logging.info("you are not in send friends page")
                            return False

    def try_goods(self,i):
        '''试戴商品'''
        logging.info("=====try goods=====")
        try:
            ivTry = self.driver.find_elements(*self.ivtry)[i]
        except NoSuchElementException:
            logging.info("can not find the try button")
            return False
        else:
            logging.info("click try")
            ivTry.click()
            try:
                self.driver.find_element(*self.ivclose)
            except NoSuchElementException:
                logging.info("can't see try on page")
                return False
            else:
                logging.info("You can see the effect of trying it on")
                return True

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = StorePageView(appium_desired(yaml_address))
    l.go_Me()
    l.go_Store()
    # l.switch_store()
    # l.switch_store_tab("Mounts")
    # l.switch_mine()
    # l.switch_mine_tab("Chat Box")
    l.buy_goods(1)
    l.buy_success_twice_window(option="later")
    # l.check_mine_page()
    l.check_store_page()
    # l.send_goods(0,option="cancel")
import os
from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.meWallet.WalletPageView import WalletPageView
import logging,time

class DiamondPageView(WalletPageView):

    diamond_record = (By.ID,'com.test.voice.chat:id/btn_right')
    diamond_exchange = (By.ID,'com.test.voice.chat:id/diamond_change_button')
    diamond_balance = (By.ID,'com.test.voice.chat:id/diamond_balance')
    diamond_FAQ = (By.ID,'com.test.voice.chat:id/tvFaq')
    exchange_diamond_balance = (By.ID,'com.test.voice.chat:id/diamond_exchange_diamond_balance')
    exchange_coins_balance = (By.ID,'com.test.voice.chat:id/diamond_exchange_coin_balance')
    exchange = (By.ID,'com.test.voice.chat:id/diamond_exchange')
    sure = (By.ID,'com.test.voice.chat:id/btn_send')
    cancel = (By.ID,'com.test.voice.chat:id/btn_cancel')
    exchange_diamond_number = (By.ID,'com.test.voice.chat:id/diamond_number')
    exchange_coins_number = (By.ID,'com.test.voice.chat:id/coin_number')

    def go_Diamond_Record(self):
        '''进入钻石记录页面'''
        logging.info("====go to diamond record page====")
        try:
            diamond_record = self.driver.find_element(*self.diamond_record)
        except NoSuchElementException:
            logging.info("can not find the diamond record")
            return False
        else:
            diamond_record.click()
            try:
                record_sign = self.driver.find_element(*self.record_sign)
            except NoSuchElementException:
                logging.info("you are not in Coins records page")
                return False
            else:
                if record_sign.get_attribute("text") == "Coins Records":
                    logging.info("go to Coins record success")
                    return True
                elif record_sign.get_attribute("text") == "No records":
                    logging.info("go to Coins record success,but no records")
                    return True
                else:
                    logging.info("you are not in Coins record page")
                    return False

    def go_Coins_Exchange(self):
        '''进入钻石兑换页面'''
        logging.info("====go to diamond exchange page====")
        try:
            diamond_exchange = self.driver.find_element(*self.diamond_exchange)
        except NoSuchElementException:
            logging.info("can not find the diamond exchange")
            return False
        else:
            diamond_exchange.click()
            try:
                record_sign = self.driver.find_element(*self.record_sign)
            except NoSuchElementException:
                logging.info("you are not in Coins records page")
                return False
            else:
                if record_sign.get_attribute("text") == "Exchange Coins":
                    logging.info("go to Coins Exchange page success")
                    return True
                else:
                    logging.info("you are not in Coins Exchange page")
                    return False

    def get_diamond_balance(self):
        '''获取钻石余额'''
        logging.info("====get DiamondPage balance")
        try:
            balance = self.driver.find_element(*self.diamond_balance)
        except NoSuchElementException:
            logging.info("can not find diamond balance")
            return False
        else:
            diamond_balance = float(balance.get_attribute("text"))
            return diamond_balance

    def get_exchange_diamond_balance(self):
        '''获取金币转换页钻石余额'''
        logging.info("====get exchange diamond balance====")
        try:
            balance = self.driver.find_element(*self.exchange_diamond_balance)
        except NoSuchElementException:
            logging.info("can not find exchange diamond balance")
            return False
        else:
            exchange_diamond_balance = float(balance.get_attribute("text"))
            return exchange_diamond_balance

    def get_exchange_coins_balance(self):
        '''获取金币转换页金币余额'''
        logging.info("====get exchange coins balance====")
        try:
            balance = self.driver.find_element(*self.exchange_coins_balance)
        except NoSuchElementException:
            logging.info("can not find exchange coins balance")
            return False
        else:
            exchange_coins_balance = float(balance.get_attribute("text"))
            return exchange_coins_balance

    def diamonds_to_coins(self,i):
        '''10个钻石兑换35个金币'''
        logging.info("====start diamonds exchange coins====")
        try:
            exchange = self.driver.find_elements(*self.exchange)[i]
        except NoSuchElementException:
            logging.info("can not find the exchange button")
            return False
        else:
            exchange.click()
            return True

    def twice_window(self,status):
        if status == True:
            try:
                sure_button = self.driver.find_element(*self.sure)
            except NoSuchElementException:
                logging.info("can not find the sure button in twice window")
                return False
            else:
                sure_button.click()
                return True
        elif status == False:
            try:
                cancel_button = self.driver.find_element(*self.cancel)
            except NoSuchElementException:
                logging.info("can not find the cancel button in twice window")
                return False
            else:
                cancel_button.click()
                return True
        else:
            logging.info("input status is not available")
            return False

    def get_exchange_diamonds_number(self,i):
        '''获取兑换减少的钻石数'''
        try:
            diamond_number = self.driver.find_elements(*self.exchange_diamond_number)[i]
        except NoSuchElementException:
            logging.info("can not find the exchange diamond number")
            return False
        else:
            exchange_diamond_number = float(diamond_number.get_attribute("text"))
            return exchange_diamond_number

    def get_exchange_coins_number(self,i):
        '''获取兑换增加的金币数'''
        try:
            coins_number = self.driver.find_elements(*self.exchange_coins_number)[i]
        except NoSuchElementException:
            logging.info("can not find the exchange coins number")
            return False
        else:
            exchange_coins_number = float(coins_number.get_attribute("text"))
            return exchange_coins_number


    def check_exchange_result(self,before_coins,before_diamonds,exchange_coins,exchange_diamonds,after_coins,after_diamonds):
        '''检测兑换结果是否成功'''
        logging.info("====diamond exchange coins success or fail====")
        if before_coins + exchange_coins == after_coins:
            logging.info("exchange coins is right")
            if before_diamonds - exchange_diamonds == after_diamonds:
                logging.info("exchange success!")
                logging.info("the increase coins is " + str(exchange_coins))
                logging.info("the decrease diamonds is " + str(round(exchange_diamonds,2)))
                return True
            else:
                logging.info("exchange diamonds is wrong")
                return False
        else:
            logging.info("exchange coins is wrong")
            return False

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = DiamondPageView(appium_desired(yaml_address))
    l.go_Me()
    D10_to_C99 = 0
    D20_to_C198 = 1
    D30_to_C297 = 2
    D50_to_C495= 3
    D100_to_C990 = 4
    # D50000_to_C15000 = 5
    # l.back_to_mePage()
    l.go_Diamonds()
    # l.go_Diamond_Record()
    l.go_Coins_Exchange()

    before_coins = l.get_exchange_coins_balance()
    before_diamonds = l.get_exchange_diamond_balance()

    exchange_coins = l.get_exchange_coins_number(D100_to_C990)
    exchange_diamonds = l.get_exchange_diamonds_number(D100_to_C990)

    l.diamonds_to_coins(D100_to_C990)
    l.twice_window(True)

    after_coins = l.get_exchange_coins_balance()
    after_diamonds = l.get_exchange_diamond_balance()

    l.check_exchange_result(before_diamonds=before_diamonds,before_coins=before_coins,exchange_coins=exchange_coins,
                            exchange_diamonds=exchange_diamonds,after_coins=after_coins,after_diamonds=after_diamonds)

    print("before coins: " + str(before_coins))
    print("before diamonds: " + str(round(before_diamonds,2)))
    print("exchange coins: " + str(exchange_coins))
    print("exchange diamonds: " + str(round(exchange_diamonds,2)))
    print("after coins: " + str(after_coins))
    print("after diamonds: " + str(round(after_diamonds,2)))

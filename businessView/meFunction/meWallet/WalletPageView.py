import os
from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.MePageVIew import MePageView
import logging,time

class WalletPageView(MePageView):

    coins_balabnce = (By.ID,'com.test.voice.chat:id/wallet_balance')
    coins_record = (By.ID,'com.test.voice.chat:id/action_record')
    record_sign = (By.ID,'com.test.voice.chat:id/title')
    coins = (By.ID,'com.test.voice.chat:id/tvGold')
    buy = (By.ID,'com.android.vending:id/footer_placeholder')
    # gold_number = (By.ID,'com.test.voice.chat:id/item_gold_number')
    add_coin = (By.ID,'com.test.voice.chat:id/tvDiscount')
    recharge_way = (By.ID,'com.test.voice.chat:id/arrow')

    def back_to_mePage(self):
        # self.go_Wallet()
        logging.info("====back to mePage====")
        # os.system("adb shell input keyevent 4")
        l.back()
        try:
            self.driver.find_element(*self.me_header)
        except NoSuchElementException:
            logging.info('back to MePage Failed')
            return False
        else:
            logging.info('back to MePage Successed')
            return True

    def go_Coins_Record(self):
        # self.go_Wallet()
        logging.info("====go to coins record page====")
        try:
            coins_record = self.driver.find_element(*self.coins_record)
        except NoSuchElementException:
            logging.info("can not find the record button")
            return False
        else:
            coins_record.click()
            try:
                record_sign = self.driver.find_element(*self.record_sign)
            except NoSuchElementException:
                logging.info("you are not in Coins records page")
                return False
            else:
                if record_sign.get_attribute("text") == "Coins Records":
                    logging.info("go to Coins record success")
                    return True
                elif record_sign.get_attribute("text") == "No records":
                    logging.info("go to Coins record success,but no records")
                    return True
                else:
                    logging.info("you are not in Coins record page")
                    return False

    def select_recharge_way(self,way):
        '''选择充值方法'''
        logging.info("=====select recharge way=====")
        if way == "google":
            i = 0
        try:
            way = self.driver.find_elements(*self.recharge_way)[i]
        except NoSuchElementException:
            logging.info("can not find the recharge way")
            return False
        else:
            logging.info("recharge way click")
            way.click()
            try:
                self.driver.find_elements(*self.coins)
            except NoSuchElementException:
                logging.info("Can't see recharge selection")
                return False
            else:
                logging.info("now have recharge selection")
                return True

    def click_Coin_toBuy(self,i):
        '''
        :param i: 标识充值的金额
            0：500 coins
            1: 1000 coins
            2: 2000 coins
            3: 5000 coins
            4: 10000 coins
        :return:
        '''
        # self.go_Wallet()
        logging.info("====click coin====")
        try:
            coins = self.driver.find_elements(*self.coins)[i]
        except NoSuchElementException:
            logging.info("can not find the coins")
            return False
        else:
            coins.click()
            try:
                buy = self.driver.find_element(*self.buy)
            except NoSuchElementException:
                logging.info("can not find the buy button")
                return False
            else:
                logging.info("click to recharge")
                buy.click()
                logging.info("pay success")
                time.sleep(10)
                return True,i

    def get_coin_balance(self):
        '''获取金币余额'''
        logging.info("====get coins balance====")
        try:
            coins_balance = self.driver.find_element(*self.coins_balabnce)
        except NoSuchElementException:
            logging.info("can not find the coins balance")
            return False
        else:
            balance_number = int(coins_balance.get_attribute("text"))
            return balance_number

    def get_recharge(self,i):
        '''获取充值金额'''
        logging.info("====get recharge coin====")
        try:
            recharge_coin = self.driver.find_elements(*self.coins)[i]
        except NoSuchElementException:
            logging.info("can not find the gold number")
            return False
        else:
            try:
                add_coin = self.driver.find_elements(*self.add_coin)[i]
            except NoSuchElementException:
                logging.info("can not find the add coin")
                return False
            else:
                if add_coin.get_attribute("text") == " ":
                    logging.info("no add coin")
                    recharge_number = int(recharge_coin.get_attribute("text"))
                    return recharge_number
                else:
                    # logging.info("add coin is "+":"+add_coin)
                    coin_add = int(add_coin.get_attribute("text"))
                    recharge_number = int(recharge_coin.get_attribute("text"))
                    total_recharge = coin_add+recharge_number
                    return total_recharge

    def check_recharge(self,coin_before,coin_recharge,coin_after):
        '''检测充值是否成功'''
        logging.info("====check recharge success or fail====")
        if coin_before + coin_recharge == coin_after:
            logging.info("recharge success! the recharge coin is "+str(coin_recharge))
            logging.info("after recharge , the coin balance is "+str(coin_after))
            return True
        else:
            logging.info("recharge coin doesn't match. recharge failed")
            return False

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = WalletPageView(appium_desired(yaml_address))
    # l.back_to_mePage()
    # l.go_Coins_Record()
    l.go_Me()
    l.go_Wallet()
    i = 1
    coin_before = l.get_coin_balance()
    l.select_recharge_way(way="google")
    coin_recharge = l.get_recharge(i)
    l.click_Coin_toBuy(i)
    time.sleep(10)
    coin_after = l.get_coin_balance()
    l.check_recharge(coin_before,coin_recharge,coin_after)
    logging.info("coin_berfore: "+str(coin_before))
    logging.info("recharge coin: "+str(coin_recharge))
    logging.info("coin_after: "+str(coin_after))
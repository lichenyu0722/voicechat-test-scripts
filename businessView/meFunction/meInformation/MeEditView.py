from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.meInformation.MeInformationView import MeInformationView
from common.common_fun import Common
import logging,time,random,string

class MeEditView(MeInformationView,Common):
    etBio = (By.ID,'com.test.voice.chat:id/tvBio')
    etCountry = (By.ID,'com.test.voice.chat:id/tvCountry')
    etName = (By.ID,'com.test.voice.chat:id/etName')
    etBirthday = (By.ID,'com.test.voice.chat:id/tvBirthDay')
    birthday_save = (By.ID,'com.test.voice.chat:id/tv_finish')
    header = (By.ID,'com.test.voice.chat:id/ivAvatar')
    photo_page = (By.ID,'com.test.voice.chat:id/albumMediaRecyclerView')
    photo =(By.ID,'com.test.voice.chat:id/media_thumbnail')
    country_list = (By.ID,'com.test.voice.chat:id/country_title')
    bio_text = (By.ID,'com.test.voice.chat:id/bio_edit')
    bio_edit_cancel =(By.ID,'com.test.voice.chat:id/edit_bio_back')
    bio_edit_done = (By.ID,'com.test.voice.chat:id/bio_edit_doen')
    done = (By.ID,'com.test.voice.chat:id/tvDone')
    photo_permission = (By.ID,'com.android.packageinstaller:id/permission_allow_button')
    birthday_month =(By.ID,'com.test.voice.chat:id/month')
    photo_yes = (By.ID, 'com.test.voice.chat:id/yes')

    def done_click(self):
        '''完成按钮点击'''
        logging.info('====change info and done to save====')
        try:
            done = self.driver.find_element(*self.done)
        except NoSuchElementException:
            logging.info('can not find the done')
            return False
        else:
            done.click()
            time.sleep(2)
            try:
                self.driver.find_element(*self.me_information_photo)
            except NoSuchElementException:
                logging.info('your edit save failed')
                # self.getScreenShot('Saved failed photo')
                return False
            else:
                logging.info('your edit save success')
                return True
    def username_edit(self,username):
        '''用户名修改'''
        # self.go_Me_information_edit()
        logging.info("====username edit====")
        try:
            username_edit = self.driver.find_element(*self.etName)
        except NoSuchElementException:
            logging.info('can not find the username edit')
            return False
        else:
            username_edit.clear()
            username_edit.send_keys(username)
            if username_edit.get_attribute('text') == username:
                logging.info('username input:'+username)
                return True
            else:
                logging.info('you input somethng wrong')
                return False
    def birthday_edit(self):
        '''生日编辑'''
        # self.go_Me_information_edit()
        logging.info("====edit birthday====")
        try:
            birthday_edit = self.driver.find_element(*self.etBirthday)
        except NoSuchElementException:
            logging.info('can not find the birthday edit button')
            return False
        else:
            birthday_edit.click()
            return True

    def birthday_select(self):
        time.sleep(1)
        self.swipeLeftDown()
        self.swipeMiddleDown()
        self.swipeRightDown()

    def birthday_saved(self):
        try:
            birthday_save = self.driver.find_element(*self.birthday_save)
        except NoSuchElementException:
            logging.info('can not find the save button')
            return False
        else:
            birthday_save.click()
            birthday = (self.driver.find_element(*self.etBirthday)).get_attribute('text')
            logging.info('birthday set：'+birthday)
            return True

    def country_edit(self):
        '''国家选择'''
        # self.go_Me_information_edit()
        logging.info("====edit country====")
        self.swipeDown(a1=500, a2=500, b1=1800, b2=40)
        try:
            country_edit = self.driver.find_element(*self.etCountry)
        except NoSuchElementException:
            logging.info('can not find the country edit button')
            return False
        else:
            country_edit.click()
            try:
                self.driver.find_element(*self.country_list)
            except NoSuchElementException:
                logging.info('you are not in country list')
                return False
            else:
                self.swipeMiddleDown()
                ran_number = random.randint(0,9)
                try:
                    goal_country = self.driver.find_elements(*self.country_list)[ran_number]
                except NoSuchElementException:
                    logging.info('can not find goal country')
                    return False
                else:
                    goal_country_text = goal_country.get_attribute('text')
                    goal_country.click()
                    time.sleep(1)
                    if country_edit.get_attribute('text') == goal_country_text:
                        logging.info('country change to goal country success')
                        return True
                    else:
                        logging.info('country change failure')
                        return False
    def header_edit(self):
        '''头像编辑'''
        # self.go_Me_information_edit()
        logging.info("====edit header photo====")
        try:
            header = self.driver.find_element(*self.header)
        except NoSuchElementException:
            logging.info('can not find the header')
            return False
        else:
            header.click()
            try:
                allow = self.driver.find_element(*self.photo_permission)
            except NoSuchElementException:
                logging.info('you already get photo permission')
                pass
            else:
                logging.info('get photo permission')
                allow.click()
            try:
                self.driver.find_element(*self.photo_page)
            except NoSuchElementException:
                logging.info("you are not in photo page")
                return False
            else:
                ran_number = random.randint(0,8)
                # print(ran_number)
                try:
                    photo = self.driver.find_elements(*self.photo)[0]
                except NoSuchElementException:
                    logging.info('can not find the photo')
                    return False
                else:
                    photo.click()
                    try:
                        self.driver.find_element(*self.photo_yes)
                    except NoSuchElementException:
                        logging.info("click again")
                        photo.click()
                    else:
                        logging.info("photo cut")
                        try:
                            photo_yes = self.driver.find_element(*self.photo_yes)
                        except:
                            logging.info("can not find photo_yes button")
                            return False
                        else:
                            photo_yes.click()
                            logging.info("photo_yes button click,selected photo ready")
                            return True

    def bio_edit(self,ran_str):
        '''个人简介编辑'''
        # self.go_Me_information_edit()
        logging.info("====edit bio====")
        self.swipeDown(a1=500,a2=500,b1=1800,b2=40)
        try:
            bio_edit = self.driver.find_element(*self.etBio)
        except NoSuchElementException:
            logging.info('can not find the bio edit button')
            return False
        else:
            bio_edit.click()
            try:
                bio_text = self.driver.find_element(*self.bio_text)
            except NoSuchElementException:
                logging.info('you are not in bio edit page')
                return False
            else:
                logging.info('you are in bio edit page')
                bio_text.clear()
                #生成一串随机字符
                bio_text.send_keys(ran_str)
                try:
                    done = self.driver.find_element(*self.bio_edit_done)
                except NoSuchElementException:
                    logging.info('can not find the done button')
                    return False
                else:
                    done.click()
                    time.sleep(1)
                    try:
                        self.driver.find_element(*self.done)
                    except NoSuchElementException:
                        logging.info("done click failed")
                        return False
                    else:
                        if bio_edit.get_attribute('text') == ran_str:
                            logging.info('you update bio success')
                            return True
                        else:
                            logging.info('you update bio failed')
                            return False
if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = MeEditView(appium_desired(yaml_address))
    l.go_Me()
    l.go_Me_information()
    l.go_Me_information_edit()
    # ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
    # l.username_edit(ran_name)
    # l.birthday_edit()
    l.country_edit()
    # l.header_edit()
    # ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 20))
    # # ran_str = "Y6FxbBlNXFSUSF9d5TrQk9MSIpSN1zKf9awE3aADGxGmLK5Ph90KAcc1C2ycJgcAf4DHIGaCTqWV5c5fdZgoRFZAL9cBUrl6FPOlnFuqZNxaGCGLvLvjobqJGzFxVdjNufYeqztueurMvO"
    # l.bio_edit(ran_str)
    l.done_click()
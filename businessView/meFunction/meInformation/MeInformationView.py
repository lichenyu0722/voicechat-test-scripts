from common.desired_caps import appium_desired
from businessView.meFunction.MePageVIew import MePageView
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging

class MeInformationView(MePageView):

    back_button = (By.XPATH,'//android.widget.ImageButton')
    me_information_edit = (By.ID,'com.test.voice.chat:id/action_edit')
    me_edit_avator = (By.ID,'com.test.voice.chat:id/ivAvatar')
    me_username = (By.ID, 'com.test.voice.chat:id/info_name')
    me_bio = (By.ID, 'com.test.voice.chat:id/info_intro')


    def back_to_mePage(self):
        '''返回到Me页面'''
        # self.go_Me_information()
        logging.info("====back to mePage====")
        try:
            back = self.driver.find_element(*self.back_button)
        except NoSuchElementException:
            logging.info('can not find the back button')
            return False
        else:
            back.click()
            try:
                self.driver.find_element(*self.me_header)
            except NoSuchElementException:
                logging.info('back to MePage Failed')
                return False
            else:
                logging.info('back to MePage Successed')
                return True

    def go_Me_information_edit(self):
        '''进入到个人信息编辑页'''
        # self.go_Me_information()
        logging.info("====go to edit my information====")
        try:
            me_edit = self.driver.find_element(*self.me_information_edit)
        except NoSuchElementException:
            logging.info('can not find the edit button')
            return False
        else:
            me_edit.click()
            try:
                self.driver.find_element(*self.me_edit_avator)
            except NoSuchElementException:
                logging.info('go to me edit information Failed')
                return False
            else:
                logging.info('you are in edit page')
                return True

    def get_Me_username(self):
        '''获取个人资料的用户名'''
        logging.info("====get my username====")
        try:
            me_username = self.driver.find_element(*self.me_username)
        except NoSuchElementException:
            logging.info("can not find my username")
            return False
        else:
            logging.info("get me username")
            return me_username.get_attribute("text")

    def get_Me_bio(self):
        '''获取个人用户简介'''
        logging.info("====get my bio====")
        try:
            me_bio = self.driver.find_element(*self.me_bio)
        except NoSuchElementException:
            logging.info("can not find my bio")
            return False
        else:
            logging.info("get me bio")
            return me_bio.get_attribute("text")

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = MeInformationView(appium_desired(yaml_address))
    # l.back_to_mePage()
    l.go_Me()
    l.go_Me_information()
    print(l.get_Me_username())
    print(l.get_Me_bio())
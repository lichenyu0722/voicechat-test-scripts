from common.desired_caps import appium_desired
from businessView.homeFunction.HomeView import HomeView
from common.common_fun import Common
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging,os

class MePageView(HomeView,Common):
    me_header = (By.ID, 'com.test.voice.chat:id/me_header_image')
    me_information_photo = (By.ID,'com.test.voice.chat:id/action_edit')
    friensgroup = (By.ID,'com.test.voice.chat:id/text_name')
    mepagegroup = (By.ID,'com.test.voice.chat:id/item_title')
    wallet_page = (By.ID,'com.test.voice.chat:id/wallet_recycler')
    diamonds_change = (By.ID,'com.test.voice.chat:id/diamond_change_button')
    level = (By.ID,'com.test.voice.chat:id/tv_cur_level')
    relationship_TAP =(By.CLASS_NAME,'androidx.appcompat.app.ActionBar.Tab')
    relationship_sign = (By.ID, 'com.test.voice.chat:id/title')
    log_out = (By.ID,'com.test.voice.chat:id/logout')
    my_invitation = (By.ID,'com.test.voice.chat:id/my_invitation_friend')
    invite_entity = (By.ID,'com.test.voice.chat:id/invite_entity')
    task_icon = (By.ID,'com.test.voice.chat:id/ivTaskIcon')
    settings = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/me_recycler_view"]/android.view.ViewGroup[13]')
    first_recharge = (By.XPATH,'//*[@text="Recharge Now"]')

    def go_Me_information(self):
        '''进入我的个人信息页'''
        # self.go_Me()
        logging.info("====click my header====")
        try:
            me_header = self.driver.find_element(*self.me_header)
        except NoSuchElementException:
            logging.info('can not find my header')
            return False
        else:
            me_header.click()
            try:
                self.driver.find_element(*self.me_information_photo)
            except NoSuchElementException:
                logging.info('you are not in me_information_page')
                return False
            else:
                logging.info('you are in me_information_page')
                return True

    def open_sign_window(self):
        '''打开每日签到弹窗'''
        logging.info("open everyday sign window")
        try:
            sign = self.driver.find_element(*self.invite_entity)
        except NoSuchElementException:
            logging.info("can not find the sign button")
            return False
        else:
            logging.info("open sign window")
            sign.click()
            self.check_sign_window()

    def go_Friends(self):
        '''进入朋友列表'''
        # self.go_Me()
        logging.info("====go friends====")
        try:
            friends = self.driver.find_elements(*self.friensgroup)[0]
        except NoSuchElementException:
            logging.info('can not find friends')
            return False
        else:
            logging.info("====click friends====")
            friends.click()
            try:
                friends_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find friends sign")
                return False
            else:
                if friends_sign.get_attribute('text') == 'Friends':
                    logging.info('you are in relationship-FRINEDS')
                    return True
                else:
                    logging.info('you are not in relationship-FRIENDS')
                    return False

    def go_Following(self):
        '''进入正在关注列表'''
        # self.go_Me()
        logging.info("====go following====")
        try:
            followings = self.driver.find_elements(*self.friensgroup)[1]
        except NoSuchElementException:
            logging.info('can not find the followings')
            return False
        else:
            logging.info("====click followings====")
            followings.click()
            try:
                following_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find the following sign")
                return False
            else:
                if following_sign.get_attribute('text') == 'Followings':
                    logging.info('you are in relationship-FOLLOWINGS')
                    return True
                else:
                    logging.info('you are not in relationship-FOLLOWINGS')
                    return False

    def go_Fans(self):
        '''进入粉丝列表'''
        # self.go_Me()
        logging.info("====go fans====")
        try:
            fans = self.driver.find_elements(*self.friensgroup)[2]
        except NoSuchElementException:
            logging.info('can not find the fans')
            return False
        else:
            logging.info("====click fans====")
            fans.click()
            try:
                fans_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find the fans sign")
                return False
            else:
                if fans_sign.get_attribute('text') == 'Fans':
                    logging.info('you are in relationship-FANS')
                    return True
                else:
                    logging.info('you are not in relationship-FANS')
                    return False

    def go_Wallet(self):
        '''进入钱包页面'''
        logging.info("====go Wallet====")
        # self.go_Me()
        try:
            wallet = self.driver.find_elements(*self.mepagegroup)[0]
        except NoSuchElementException:
            logging.info('can not find the wallet')
            return False
        else:
            wallet.click()

            '''首充页面判断'''
            try:
                self.driver.find_element(*self.first_recharge)
            except NoSuchElementException:
                logging.info("you had recharge")
                pass
            else:
                logging.info("you are not recharge,physical back")
                os.system("adb shell input keyevent 4")
            '''首充页面判断'''
            try:
                self.driver.find_element(*self.wallet_page)
            except NoSuchElementException:
                logging.info('you are not in wallet page')
                return False
            else:
                logging.info('you are in wallet page')
                return True

    def go_Diamonds(self):
        '''进入钻石兑换页面'''
        # self.go_Me()
        logging.info("====go Diamonds====")
        try:
            diamonds = self.driver.find_elements(*self.mepagegroup)[1]
        except NoSuchElementException:
            logging.info('can not find the diamonds')
            return False
        else:
            diamonds.click()
            try:
                self.driver.find_element(*self.diamonds_change)
            except NoSuchElementException:
                logging.info('you are not in diamonds change page')
                return False
            else:
                logging.info('you are in diamonds change page')
                return True

    def go_Aristocracy(self):
        '''进入贵族中心页面'''
        logging.info("=====go Aristocracy=====")
        try:
            aristocracy = self.driver.find_elements(*self.mepagegroup)[2]
        except NoSuchElementException:
            logging.info("can not find the aristocracy")
            return False
        else:
            aristocracy.click()
            try:
                Badge = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("you are not in Aristocracy page")
                return False
            else:
                if Badge.get_attribute("text") == "Aristocracy":
                    logging.info("now you are in Aristocracy page")
                    return True
                else:
                    logging.info("you are not in Aristocracy page")
                    return False


    def go_coin_task(self):
        '''进入金币任务页面'''
        logging.info("====go Coin task====")
        try:
            coin_task = self.driver.find_elements(*self.mepagegroup)[5]
        except NoSuchElementException:
            logging.info("can not find the coin task")
            return False
        else:
            coin_task.click()
            try:
                self.driver.find_element(*self.task_icon)
            except NoSuchElementException:
                logging.info("you are not in Coin Task Page")
                return False
            else:
                logging.info("you are in Coin Task Page")
                return True

    def go_Level(self):
        '''进入等级页'''
        logging.info("====go Level====")
        try:
            level = self.driver.find_element(*self.settings)
        except NoSuchElementException:
            logging.info('can not find the level')
            return False
        else:
            level.click()
            try:
                self.driver.find_element(*self.level)
            except NoSuchElementException:
                logging.info('you are not in user level page')
                return False
            else:
                logging.info('you are in user level page')
                self.getScreenShot("Level Page Screenshot")
                return True

    def go_Badge(self):
        '''进入勋章页'''
        logging.info("====go Badge====")
        try:
            badge = self.driver.find_elements(*self.mepagegroup)[7]
        except NoSuchElementException:
            logging.info("can not find the badge")
            return False
        else:
            logging.info("click to go Badge page")
            badge.click()
            try:
                Badge = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("you are not in Badge page")
                return False
            else:
                if Badge.get_attribute("text") == "Badge":
                    logging.info("now you are in Badge page")
                    return True
                else:
                    logging.info("you are not in Badge page")
                    return False

    def go_Store(self):
        '''进入商店页面'''
        logging.info("====go Store====")
        try:
            store = self.driver.find_elements(*self.mepagegroup)[3]
        except NoSuchElementException:
            logging.info("can not find the store")
            return False
        else:
            logging.info("click to go Store page")
            store.click()
            try:
                Store = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("you are not in Store page")
                return False
            else:
                if Store.get_attribute("text") == "Store":
                    logging.info("now you are in Store page")
                    return True
                else:
                    logging.info("you are not in Store page")
                    return False

    def go_invite_code_page(self):
        '''进入邀请码页面'''
        logging.info("====go invite code page====")
        try:
            invite = self.driver.find_elements(*self.mepagegroup)[4]
        except NoSuchElementException:
            logging.info("can not find the invite")
            return False
        else:
            logging.info("click to go invite code page")
            invite.click()
            try:
                self.driver.find_element(*self.my_invitation)
            except NoSuchElementException:
                logging.info("you are not in invite code page")
                return False
            else:
                logging.info("now you are in invite code page")
                return True

    def go_Feedback(self):
        '''进入反馈页面'''
        logging.info("====go Feedback====")
        self.swipeDown(a1=500,a2=500,b1=1700,b2=900)
        try:
            feedback = self.driver.find_elements(*self.mepagegroup)[8]
        except NoSuchElementException:
            logging.info("can not find the feedback")
            return False
        else:
            logging.info("click to go feedback")
            feedback.click()
            try:
                FAQ = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("you are not in FAQ page")
                return False
            else:
                if FAQ.get_attribute("text") == "FAQ":
                    logging.info("now you are in FAQ page")
                    return True
                else:
                    logging.info("you are not in FAQ page")
                    return False

    def go_Setting(self):
        '''进入设置页面'''
        logging.info("====go Setting====")
        self.swipeDown(a1=500,a2=500,b1=1700,b2=900)
        try:
            settings = self.driver.find_elements(*self.mepagegroup)[9]
        except NoSuchElementException:
            logging.info('can not find the settings')
            return False
        else:
            settings.click()
            try:
                self.driver.find_element(*self.log_out)
            except NoSuchElementException:
                logging.info("you are not in settings page")
                return False
            else:
                logging.info("you are in settings page")
                return True



if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = MePageView(appium_desired(yaml_address))
    l.go_Me()
    # l.go_coin_task()
    l.swipeDown(a1=500,a2=500,b1=1500,b2=100)
    l.go_Badge()
    l.back()
    l.go_invite_code_page()
    l.back()
    # l.open_sign_window()
    l.go_Feedback()
    l.back()
    # l.go_Store()
    # l.go_invite_code_page()
    # l.go_Me_information()
    # l.go_Friends()
    # l.go_Following()
    # l.go_Fans()
    # l.go_Wallet()
    # l.go_Diamonds()
    # l.go_Level()
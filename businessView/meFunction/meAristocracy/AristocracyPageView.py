from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.MePageVIew import MePageView
import logging,time

class AristocracyPageView(MePageView):

    settingButton = (By.ID,'com.test.voice.chat:id/btn_right')
    buy = (By.ID,'com.test.voice.chat:id/tvBuy')
    send = (By.ID,'com.test.voice.chat:id/tvSend')
    viscount = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]/'
                         'android.widget.LinearLayout[1]/android.widget.LinearLayout[1]')
    count = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]/'
                      'android.widget.LinearLayout[1]/android.widget.LinearLayout[2]')
    marquis = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]/'
                        'android.widget.LinearLayout[1]/android.widget.LinearLayout[3]')
    duke = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabLayout"]/'
                     'android.widget.LinearLayout[1]/android.widget.LinearLayout[4]')
    description = (By.ID,'com.test.voice.chat:id/tvAristocracyInfo')
    sure_buy = (By.ID,'com.test.voice.chat:id/dialog_positive')
    cancel_buy = (By.ID,'com.test.voice.chat:id/dialog_nagative')
    mydict = {"viscount": viscount, "count": count, "marquis": marquis, "duke": duke}

    def select_aristocracy(self,aristocracy):
        '''选择贵族'''
        logging.info("=====select aristorcracy=====")
        i = 0
        lt = list(self.mydict)
        while i<len(lt):
            if aristocracy == lt[i]:
                logging.info("=====select %s====="%(lt[i]))
                try:
                    viscount = self.driver.find_element(*self.mydict[lt[i]])
                except NoSuchElementException:
                    logging.info("can not find %s selection"%(lt[i]))
                    return False
                else:
                    logging.info("=====check selected or not=====")
                    if viscount.get_attribute("clickable") == "false":
                        logging.info("you already select %s"%(lt[i]))
                        return True
                    elif viscount.get_attribute("clickable") == "true":
                        logging.info("select %s"%(lt[i]))
                        viscount.click()
                        if viscount.get_attribute("clickable") == "false":
                            logging.info("select %s success"%(lt[i]))
                            return True
                        else:
                            logging.info("select %s failed"%(lt[i]))
                            return False
                    else:
                        logging.info("something wrong")
                        return False
            i = i+1

    def check_for_aristocracy(self,aristocracy):
        '''检测是否为贵族'''
        logging.info("=====check for Aristocracy=====")
        try:
            description = self.driver.find_element(*self.description)
        except NoSuchElementException:
            logging.info("can not find the description")
            return False
        else:
            i = 0
            lt = list(self.mydict)
            while i < len(lt):
                if aristocracy == lt[i]:
                    if "You are not " in description.get_attribute("text"):
                        logging.info("you are not %s Aristocracy"%lt[i])
                        return False
                    else:
                        logging.info("you are %s"%lt[i])
                        return True
                i = i+1

    def buy_aristocracy(self):
        '''购买贵族'''
        logging.info("=====buy aristocracy=====")
        try:
            buy = self.driver.find_element(*self.buy)
        except NoSuchElementException:
            logging.info("can not find the buy button")
            return False
        else:
            logging.info("click to buy aristocracy")
            buy.click()
            try:
                sure_buy = self.driver.find_element(*self.sure_buy)
            except NoSuchElementException:
                logging.info("can not find the twice window for buy,failed to buy")
                return False
            else:
                logging.info("sure to buy")
                sure_buy.click()
                if self.get_toast_text(text="Success") == "Success":
                    logging.info("Success")
                    return True
                elif self.get_toast_text("Can't buy lower Aristocracy") == "Can't buy lower Aristocracy":
                    logging.info("you have senior aristocracy")
                    return False
                else:
                    logging.info("something wrong")
                    return False


if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = AristocracyPageView(appium_desired(yaml_address))
    l.go_Me()
    l.go_Aristocracy()
    l.select_aristocracy("duke")
    l.buy_aristocracy()
    # if l.check_for_aristocracy(aristocracy) == True:
    #     logging.info("you are aristocracy")
    # else:
    #     logging.info("you are not aristocracy")


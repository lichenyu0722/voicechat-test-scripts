from common.desired_caps import appium_desired
from businessView.meFunction.MePageVIew import MePageView
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging

class MeSettingsView(MePageView):

    log_out = (By.ID, 'com.test.voice.chat:id/logout')
    log_out_message = (By.ID,'com.test.voice.chat:id/dialog_message')
    sure_log_out = (By.ID,'com.test.voice.chat:id/dialog_positive')
    cancel_log_out = (By.ID,'com.test.voice.chat:id/dialog_nagative')
    phone_login_button = (By.ID, 'com.test.voice.chat:id/phone_login_button')
    item_content = (By.ID,'com.test.voice.chat:id/item_content')
    language_title = (By.ID,'com.test.voice.chat:id/tvTitle')
    sharePlatform = (By.ID,'com.test.voice.chat:id/rvSharePlatform')
    dialog_message = (By.ID,'com.test.voice.chat:id/dialog_message')
    dialog_positive = (By.ID,'com.test.voice.chat:id/dialog_positive')
    dialog_nagative = (By.ID,'com.test.voice.chat:id/dialog_nagative')
    item_size = (By.ID,'com.test.voice.chat:id/item_size')

    def log_Out(self):
        '''登出操作'''
        logging.info("====log out====")
        try:
            log_out = self.driver.find_element(*self.log_out)
        except NoSuchElementException:
            logging.info("can not find log out button")
            return False
        else:
            log_out.click()
            try:
                message = self.driver.find_element(*self.log_out_message)
            except NoSuchElementException:
                logging.info("can not find twice window")
                return False
            else:
                if message.get_attribute("text") == "Sure to log out?":
                    logging.info("get twice window")
                    return True
                else:
                    logging.info("failed get twice window")
                    return False

    def settings_twice_window(self,status):
        '''登出二次弹窗点击'''
        logging.info("====log out twice window====")
        if status == True:
            try:
                sure = self.driver.find_element(*self.sure_log_out)
            except NoSuchElementException:
                logging.info("can not find the sure log out button")
                return False
            else:
                sure.click()
                logging.info("click sure to log out")
                return True
        elif status == False:
            try:
                cancel = self.driver.find_element(*self.cancel_log_out)
            except NoSuchElementException:
                logging.info("can not find the cancel log out button")
                return False
            else:
                cancel.click()
                return True

    def check_log_out_result(self):
        '''检查登出结果'''
        logging.info("====check log out result====")
        try:
            self.driver.find_element(*self.phone_login_button)
        except NoSuchElementException:
            logging.info("you are not in Logging View,log out failed")
            return False
        else:
            logging.info("log out success")
            return True

    def go_language(self):
        '''进入语言设置页面'''
        logging.info("====go language====")
        try:
            language = self.driver.find_elements(*self.item_content)[0]
        except NoSuchElementException:
            logging.info("can not find the language")
            return False
        else:
            logging.info("click the language button")
            language.click()
            try:
                language_title = self.driver.find_element(*self.language_title)
            except NoSuchElementException:
                logging.info("you are not in language page")
                return False
            else:
                if language_title.get_attribute("text") == "Language":
                    logging.info("you are in language page now")
                    return True
                else:
                    logging.info("you are not in language page ")
                    return False

    def go_share(self):
        '''分享APP'''
        logging.info("====go share====")
        try:
            share = self.driver.find_elements(*self.item_content)[2]
        except NoSuchElementException:
            logging.info("can not find the share")
            return False
        else:
            logging.info("click the share button")
            share.click()
            try:
                self.driver.find_element(*self.sharePlatform)
            except NoSuchElementException:
                logging.info("can not find the share window")
                return False
            else:
                logging.info("get share APP window")
                return True

    def clear_cache(self):
        '''清除应用数据'''
        logging.info("====clear cache====")
        try:
            clear = self.driver.find_elements(*self.item_content)[3]
        except NoSuchElementException:
            logging.info("can not find the clear option")
            return False
        else:
            logging.info("clear cache")
            clear.click()
            try:
                twice_message = self.driver.find_element(*self.dialog_message)
            except NoSuchElementException:
                logging.info("can not find the clear twice window")
                return False
            else:
                if twice_message.get_attribute("text") == "Sure to clear cache?":
                    logging.info("get twice window")
                    return True
                else:
                    logging.info("can not find the clear twice window")
                    return False

    def get_cache(self):
        '''获取手机应用数据'''
        logging.info("====get cache====")
        try:
            self.driver.find_elements(*self.item_size)[1]
        except NoSuchElementException:
            logging.info("can not find the cache")
            return False
        else:
            cache = (self.driver.find_elements(*self.item_size)[1]).get_attribute("text")
            logging.info("the cache is "+str(cache))
            return True

    def check_cache_is_clear(self):
        '''检测cache是否clear'''
        logging.info("=====check cache is clear=====")
        try:
            self.driver.find_elements(*self.item_size)[1]
        except NoSuchElementException:
            logging.info("can not find the cache")
            return False
        else:
            cache = (self.driver.find_elements(*self.item_size)[1]).get_attribute("text")
            if cache == "0.0Byte":
                logging.info("the cache is clear successful")
                return True
            else:
                logging.info("the cache is clear failed")
                return False



if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = MeSettingsView(appium_desired(yaml_address))
    l.go_Me()
    l.go_Setting()
    l.get_cache()
    l.clear_cache()
    l.settings_twice_window(status=True)
    l.check_cache_is_clear()
    # l.go_share()
    # l.back()
    # l.go_language()
    # l.log_Out()
    # l.check_log_out_result()



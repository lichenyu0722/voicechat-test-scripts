from businessView.meFunction.MePageVIew import MePageView
from selenium.common.exceptions import NoSuchElementException
import logging,time
from common.desired_caps import appium_desired
from selenium.webdriver.common.by import By

class MessageView(MePageView):

    btn_right = (By.ID,'com.test.voice.chat:id/btn_right')
    message_tab = (By.ID,'com.test.voice.chat:id/iv_avatar')
    # title = (By.ID,'com.test.voice.chat:id/title')
    notice = (By.XPATH,'//*[@resource-id="android:id/content"]/android.widget.FrameLayout[3]/android.widget.FrameLayout[1]/'
                       'android.widget.LinearLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]')
    system = (By.XPATH,'//*[@resource-id="android:id/content"]/android.widget.FrameLayout[3]/android.widget.FrameLayout[1]/'
                       'android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.TextView[1]')
    empty = (By.ID,'com.test.voice.chat:id/tv_data_empty_tip')

    def go_relationship(self):
        '''去到用户界面'''
        logging.info("=====go friends=====")
        try:
            friends = self.driver.find_element(*self.btn_right)
        except NoSuchElementException:
            logging.info("can not find friends button in message page")
            return False
        else:
            friends.click()
            try:
                friends_FRIENDS = self.driver.find_elements(*self.relationship_TAP)[0]
            except NoSuchElementException:
                logging.info('can not find the friends_FRIENDS')
                return False
            else:
                if friends_FRIENDS.get_attribute('clickable') == 'true':
                    logging.info('you are not in friends-FRINEDS')
                    return False
                else:
                    logging.info('you are in friends-FRIENDS')
                    return True

    def go_notice(self):
        '''进入notice界面'''
        logging.info("====go notice====")
        try:
            notice = self.driver.find_elements(*self.message_tab)[0]
        except NoSuchElementException:
            logging.info("can not find the notice")
            return False
        else:
            notice.click()
            try:
                notice_title = self.driver.find_element(*self.notice)
            except NoSuchElementException:
                logging.info("you are not in notice page")
                return False
            else:
                if notice_title.get_attribute('text') == "Notice":
                    logging.info("Now you are in notice page")
                    return True
                else:
                    logging.info("you are not in notice page")
                    return False

    def notice_clear(self):
        '''notice消息清除'''
        logging.info("====notice message clear====")
        try:
            clear = self.driver.find_element(*self.btn_right)
        except NoSuchElementException:
            logging.info("can not find clear btn")
            return False
        else:
            clear.click()
            try:
                self.driver.find_element(*self.empty)
            except NoSuchElementException:
                logging.info("can not find the empty sign")
                return False
            else:
                logging.info("already clear notice message")
                return True

    def go_system(self):
        '''进入system界面'''
        logging.info("====go system====")
        try:
            system = self.driver.find_elements(*self.message_tab)[1]
        except NoSuchElementException:
            logging.info("can not find the system")
            return False
        else:
            system.click()
            try:
                system_title = self.driver.find_element(*self.system)
            except NoSuchElementException:
                logging.info("you are not in system page")
                return False
            else:
                if system_title.get_attribute('text') == "System":
                    logging.info("Now you are in system page")
                    return True
                else:
                    logging.info("you are not in system page")
                    return False

    def system_clear(self):
        '''system消息清理'''
        logging.info("====system message clear====")
        try:
            clear = self.driver.find_element(*self.btn_right)
        except NoSuchElementException:
            logging.info("can not find clear btn")
            return False
        else:
            clear.click()
            try:
                self.driver.find_element(*self.empty)
            except NoSuchElementException:
                logging.info("can not find the empty sign")
                return False
            else:
                logging.info("already clear notice message")
                return True


if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = MessageView(appium_desired(yaml_address))
    l.go_Message()
    # l.go_relationship()
    l.go_notice()
    # l.go_system()
    l.notice_clear()


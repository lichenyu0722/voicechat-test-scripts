import time
from businessView.messageFunction.MessageView import MessageView
from selenium.common.exceptions import NoSuchElementException
import logging
from common.desired_caps import appium_desired
from selenium.webdriver.common.by import By

class RelationshipView(MessageView):

    relationship_sign = (By.ID,'com.test.voice.chat:id/title')

    def switch_following(self):
        '''切换到following'''
        logging.info("====switch to following====")
        try:
            following = self.driver.find_elements(*self.relationship_TAP)[1]
        except NoSuchElementException:
            logging.info("can not find following")
            return False
        else:
            following.click()
            try:
                following_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find the following sign")
                return False
            else:
                if following_sign.get_attribute('text') == 'Followings':
                    logging.info('you are in relationship-FOLLOWINGS')
                    return True
                else:
                    logging.info('you are not in relationship-FOLLOWINGS')
                    return False

    def switch_to_fans(self):
        '''切换到fans'''
        logging.info("====switch to fans====")
        try:
            fans = self.driver.find_elements(*self.relationship_TAP)[2]
        except NoSuchElementException:
            logging.info("can not find the fans")
            return False
        else:
            fans.click()
            try:
                fans_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find the fans sign")
                return False
            else:
                if fans_sign.get_attribute('text') == 'Fans':
                    logging.info('you are in relationship-FANS')
                    return True
                else:
                    logging.info('you are not in relationship-FANS')
                    return False

    def switch_to_friends(self):
        '''切换到friends'''
        logging.info("====switch to friends====")
        try:
            friends = self.driver.find_elements(*self.relationship_TAP)[0]
        except NoSuchElementException:
            logging.info("can not find the friends")
            return False
        else:
            friends.click()
            try:
                friends_sign = self.driver.find_element(*self.relationship_sign)
            except NoSuchElementException:
                logging.info("can not find friends sign")
                return False
            else:
                if friends_sign.get_attribute('text') == 'Friends':
                    logging.info('you are in relationship-FRINEDS')
                    return True
                else:
                    logging.info('you are not in relationship-FRIENDS')
                    return False

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = RelationshipView(appium_desired(yaml_address))
    l.go_Message()
    l.go_relationship()
    l.switch_to_fans()
    time.sleep(1)
    l.switch_following()
    time.sleep(1)
    l.switch_to_friends()

from businessView.messageFunction.MessageView import MessageView
from selenium.common.exceptions import NoSuchElementException
import logging
from common.desired_caps import appium_desired
from selenium.webdriver.common.by import By

class NoticeView(MessageView):

    back_btn = (By.ID,'com.test.voice.chat:id/btn_left')
    clean_btn = (By.ID,'com.test.voice.chat:id/btn_right')
    empty_tip = (By.ID,'com.test.voice.chat:id/tv_data_empty_tip')

    def back_to_message(self):
        '''返回到message页面'''
        logging.info("====back to message page====")
        try:
            back_btn = self.driver.find_element(*self.back_btn)
        except NoSuchElementException:
            logging.info("can not find the back btn")
            return False
        else:
            back_btn.click()
            try:
                self.driver.find_element(*self.message_page_sign)
            except NoSuchElementException:
                logging.info(" you are not in message_page ")
                return False
            else:
                logging.info(" back to message_page success ")
                return True


    def clear_notice(self):
        '''清除notice消息'''
        logging.info("====clean notice message====")
        try:
            clean_btn = self.driver.find_element(*self.clean_btn)
        except NoSuchElementException:
            logging.info("can not find the clean button")
            return False
        else:
            clean_btn.click()
            try:
                self.driver.find_element(*self.empty_tip)
            except NoSuchElementException:
                logging.info("the notice is not clean up")
                return False
            else:
                logging.info("you already clean the notice message")
                return True

if __name__ == '__main__':
    yaml_address = "../../../config/desired.yaml"
    l = NoticeView(appium_desired(yaml_address))
    l.go_Message()
    l.go_notice()
    l.back_to_message()
    l.go_notice()
    l.clear_notice()
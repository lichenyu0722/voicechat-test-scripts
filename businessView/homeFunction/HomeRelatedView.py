from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from businessView.homeFunction.HomeView import HomeView
from businessView.roomFunction.master_in_room_function import MasterInRoom
import logging
from common.desired_caps import appium_desired

class HomeRelatedView(MasterInRoom):

    recently = (By.XPATH, '//*[@resource-id="com.test.voice.chat:id/home_inner_tab_layout"]/android.widget.LinearLayout[1]'
                          '/androidx.appcompat.app.ActionBar.Tab[1]/android.view.ViewGroup[1]')
    followings = (By.XPATH, '//*[@resource-id="com.test.voice.chat:id/home_inner_tab_layout"]/android.widget.LinearLayout[1]'
                            '/androidx.appcompat.app.ActionBar.Tab[2]/android.view.ViewGroup[1]')
    friends = (By.XPATH, '//*[@resource-id="com.test.voice.chat:id/home_inner_tab_layout"]/android.widget.LinearLayout[1]'
                         '/androidx.appcompat.app.ActionBar.Tab[3]/android.view.ViewGroup[1]')
    # my_room = (By.ID,'com.test.voice.chat:id/home_create_label_icon')
    my_room = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/my_room"]/android.view.ViewGroup[1]')
    def go_recently(self):
        logging.info("====switch to recently page====")
        try:
            recently = self.driver.find_element(*self.recently)
        except NoSuchElementException:
            logging.info("can not find recently button")
            return False
        else:
            recently.click()
            logging.info("now you are in recently page")
            return True

    def go_followings(self):
        logging.info("====switch to followings page====")
        try:
            followings = self.driver.find_element(*self.followings)
        except NoSuchElementException:
            logging.info("can not find followings button")
            return False
        else:
            followings.click()
            logging.info("now you are in followings page")
            return True

    def go_friends(self):
        logging.info("====switch to friends page====")
        try:
            friends = self.driver.find_element(*self.friends)
        except NoSuchElementException:
            logging.info("can not find friends button")
            return False
        else:
            friends.click()
            logging.info("now you are in friends page")
            return True

    def go_MyRoom(self):
        logging.info("=====go My Room====")
        try:
            my_room = self.driver.find_element(*self.my_room)
        except NoSuchElementException:
            logging.info("can not find the my_room")
            return False
        else:
            my_room.click()
            self.check_my_room()
            return True



if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = HomeRelatedView(appium_desired(yaml_address))
    l.go_related()
    # l.go_recently()
    # l.go_friends()
    # l.go_followings()
    l.go_MyRoom()
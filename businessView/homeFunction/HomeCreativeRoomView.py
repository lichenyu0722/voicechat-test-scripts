from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from businessView.meFunction.meInformation.MeEditView import MeEditView
from businessView.loginAndRegisterFunction.LoginView import LoginView
import logging,random,string,time

class HomeCreativeRoom(LoginView,MeEditView):

    close_button = (By.ID,'com.test.voice.chat:id/ivClose')
    creative_room_avator = (By.ID,'com.test.voice.chat:id/ivImgStroke')
    creative_room_name = (By.ID,'com.test.voice.chat:id/etRoomTitle')
    btnCreateRoom = (By.ID,'com.test.voice.chat:id/btnCreateRoom')
    roomTitle = (By.ID,'com.test.voice.chat:id/tvRoomTitle')
    label = (By.ID,'com.test.voice.chat:id/tvLabel')

    def close_CreativeRoom_page(self):
        '''关闭创建房间页面'''
        logging.info("====close creative room page====")
        try:
            close = self.driver.find_element(*self.close_button)
        except NoSuchElementException:
            logging.info("can not find the close button")
            return False
        else:
            close.click()
            try:
                self.driver.find_element(*self.home_content)
            except NoSuchElementException:
                logging.info("you are not in HomePage")
                return False
            else:
                logging.info("you are back to HomePage now")
                return True

    def select_room_avator(self):
        '''选择房间头像'''
        logging.info("====select room avator====")
        try:
            room_avator = self.driver.find_element(*self.creative_room_avator)
        except NoSuchElementException:
            logging.info("can not find the room avator")
            return False
        else:
            room_avator.click()
            try:
                allow = self.driver.find_element(*self.photo_permission)
            except NoSuchElementException:
                logging.info('you already get photo permission')
                pass
            else:
                logging.info('get photo permission')
                allow.click()
            try:
                self.driver.find_element(*self.photo_page)
            except NoSuchElementException:
                logging.info("you are not in photo page")
                return False
            else:
                ran_number = random.randint(0,8)
                # print(ran_number)
                try:
                    photo = self.driver.find_elements(*self.photo)[1]
                except NoSuchElementException:
                    logging.info('can not find the photo')
                    return False
                else:
                    photo.click()
                    try:
                        self.driver.find_element(*self.photo_yes)
                    except NoSuchElementException:
                        logging.info("click again")
                        photo.click()
                    else:
                        logging.info("photo cut")
                        try:
                            photo_yes = self.driver.find_element(*self.photo_yes)
                        except:
                            logging.info("can not find photo_yes button")
                            return False
                        else:
                            photo_yes.click()
                            logging.info("photo_yes button click,selected photo ready")
                            return True

    def check_room_creative(self,RoomName):
        '''检测创建房间是否成功'''
        logging.info("====check room creative success or fail====")
        try:
            room_name = self.driver.find_element(*self.roomTitle)
        except NoSuchElementException:
            logging.info("you are not in room")
            return False
        else:
            if room_name.get_attribute("text") == RoomName:
                logging.info("creative room success!now you are in room")
                return True
            else:
                logging.info("you are not in your own room")
                return False

    def input_room_name(self,RoomName):
        '''输入房间名'''
        logging.info("====input room name====")
        try:
            room_name = self.driver.find_element(*self.creative_room_name)
        except NoSuchElementException:
            logging.info("can not find the room name")
            return False
        else:
            room_name.clear()
            room_name.send_keys(RoomName)
            if room_name.get_attribute("text") == RoomName:
                logging.info("input room name success!the room name is :" + RoomName)
                return True
            else:
                logging.info("input room name failed")
                return False


    def select_room_label(self,i,label_name):
        '''选择房间标签'''
        logging.info("=====select room label=====")
        try:
            label = self.driver.find_elements(*self.label)[i]
        except NoSuchElementException:
            logging.info("can not find the label")
            return False
        else:
            if label.get_attribute("text") == label_name:
                logging.info("select label :"+ label_name)
                label.click()
                return True
            else:
                logging.info("select label failed")
                return False

    def creative_room_sure(self):
        '''点击Go to Live按钮'''
        logging.info("====click Go to Live button====")
        try:
            Go_to_Live = self.driver.find_element(*self.btnCreateRoom)
        except NoSuchElementException:
            logging.info("can not find the CreativeRoom btn")
            return False
        else:
            Go_to_Live.click()
            return True

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = HomeCreativeRoom(driver)
    # l.go_Me()
    # l.go_Me_information()
    # username = l.get_Me_username()
    # l.back_to_mePage()
    # l.go_Home()
    l.go_room()
    # l.select_room_avator()
    # ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 20))
    l.input_room_name(RoomName="this is my room")
    l.select_room_label(i=6,label_name="Family")
    # l.creative_room_sure()
    # time.sleep(5)
    # l.check_room_creative(RoomName=username+"'s place")


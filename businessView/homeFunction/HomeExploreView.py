from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from businessView.homeFunction.HomeHotView import HomeHotView
import logging,time
from common.desired_caps import appium_desired

class HomeExploreView(HomeHotView):

    more = (By.ID,'com.test.voice.chat:id/home_content_more_tv')
    country_title = (By.ID,'com.test.voice.chat:id/country_title')

    def go_more_country(self):
        '''进入更多国家'''
        time.sleep(5)
        self.swipeDown(a1=500,b1=1200,a2=500,b2=500)
        try:
            more = self.driver.find_element(*self.more)
        except NoSuchElementException:
            logging.info("can not find the more button")
            logging.info("swipe again")
            self.swipeDown(a1=500, b1=1200, a2=500, b2=500)
            try:
                more = self.driver.find_element(*self.more)
            except NoSuchElementException:
                logging.info("can not find the more button")
                return False
            else:
                logging.info("click more button")
                more.click()
                try:
                    title = self.driver.find_element(*self.title)
                except NoSuchElementException:
                    logging.info("you are not in more Country page")
                    return False
                else:
                    if title.get_attribute("text") == "Country":
                        try:
                            self.driver.find_element(*self.country_title)
                        except NoSuchElementException:
                            logging.info("you are not in more Country page")
                            return False
                        else:
                            logging.info("now you are in more Country page")
                            return True
                    else:
                        logging.info("you are not in more Country page")
                        return False
        else:
            logging.info("click more button")
            more.click()
            try:
                title = self.driver.find_element(*self.title)
            except NoSuchElementException:
                logging.info("you are not in more Country page")
                return False
            else:
                if title.get_attribute("text") == "Country":
                    try:
                        self.driver.find_element(*self.country_title)
                    except NoSuchElementException:
                        logging.info("you are not in more Country page")
                        return False
                    else:
                        logging.info("now you are in more Country page")
                        return True
                else:
                    logging.info("you are not in more Country page")
                    return False

    def go_more_GiftRank(self):
        '''进入大礼物榜'''
        try:
            more = self.driver.find_element(*self.more)
        except NoSuchElementException:
            logging.info("can not find the more button")
            return False
        else:
            more.click()
            logging.info("go to gift rank")
            return True
            # self.check_gift_rank()

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = HomeExploreView(appium_desired(yaml_address))
    l.go_explore()
    l.go_more_country()
    # l.go_more_GiftRank()
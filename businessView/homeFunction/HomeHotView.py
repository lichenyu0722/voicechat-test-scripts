from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from businessView.homeFunction.HomeView import HomeView
from common.common_fun import Common
import logging,time
from common.desired_caps import appium_desired

class HomeHotView(HomeView,Common):

    wealth = (By.XPATH, '//*[@content-desc="Wealth"]')
    charm = (By.XPATH, '//*[@content-desc="Charm"]')
    rooms = (By.XPATH,'//*[@content-desc="Rooms"]')
    gift = (By.XPATH,'//*[@content-desc="Gift"]')
    ranking = (By.ID,'com.test.voice.chat:id/tvRank')
    cp_ranking = (By.ID,'com.test.voice.chat:id/tvCp')
    activity = (By.ID,'com.test.voice.chat:id/activityMask')
    PK = (By.ID,'com.test.voice.chat:id/tvPk')
    room_icon = (By.ID,'com.test.voice.chat:id/home_content_icon')
    room_name = (By.ID,'com.test.voice.chat:id/home_content_title_text')
    room_inner_name = (By.ID,'com.test.voice.chat:id/tvRoomTitle')
    cpValue = (By.ID,'com.test.voice.chat:id/tvTopCpValue')
    title = (By.ID,'com.test.voice.chat:id/title')
    coin_task = (By.ID,'com.test.voice.chat:id/ivCoinTaskTreasure')
    viewTab = (By.ID,'com.test.voice.chat:id/viewTabBg')
    DailyTask = (By.XPATH,'//*[@content-desc="Daily task"]')
    ImprovementTask = (By.XPATH,'//*[@content-desc="Improvement task"]')
    recharge = (By.ID,'com.test.voice.chat:id/tvRecharge')

    def go_CP_ranking(self):
        logging.info("=====go to cp rank=====")
        try:
            cp_rank = self.driver.find_element(*self.cp_ranking)
        except NoSuchElementException:
            logging.info("can not find the ranking button")
            return False
        else:
            cp_rank.click()
            logging.info("======check CP right or wrong======")
            try:
                self.driver.find_element(*self.cpValue)
            except NoSuchElementException:
                logging.info("you are not in CP rank page")
                return False
            else:
                logging.info("now you are in CP rank page")
                return True

    def go_Activity(self):
        logging.info("=====go to Activity page=====")
        try:
            activity = self.driver.find_element(*self.activity)
        except NoSuchElementException:
            logging.info("can not find the gift_received button")
            return False
        else:
            activity.click()
            logging.info("=====check activity right or wrong=====")
            try:
                title = self.driver.find_element(*self.title)
            except NoSuchElementException:
                logging.info("can not find Activity ,you are not in Activity page")
                return False
            else:
                if title.get_attribute("text") == "Activity":
                    logging.info("you are in Activity page")
                    return True
                else:
                    logging.info("you are not in Activity page")
                    return False

    def go_Recharge(self):
        logging.info("=====go to recharge pagre=====")
        try:
            recharge = self.driver.find_element(*self.recharge)
        except NoSuchElementException:
            logging.info("can not find the recharge button")
            return False
        else:
            recharge.click()
            logging.info("====check recharge page====")

    def go_ranking(self):
        logging.info("=====go to rank: =====")
        try:
            rank = self.driver.find_element(*self.ranking)
        except NoSuchElementException:
            logging.info("can not find the rank button")
            return False
        else:
            logging.info("click to go rank")
            rank.click()
            return True

    def go_PK(self):
        logging.info("=====go to PK=====")
        try:
            pk = self.driver.find_element(*self.PK)
        except NoSuchElementException:
            logging.info("can not find the PK button")
            return False
        else:
            logging.info("click to go pk square")
            pk.click()
            return True

    def open_coin_task(self):
        '''打开Hot页的Coin task'''
        logging.info("=====open coin task=====")
        try:
            icon = self.driver.find_element(*self.coin_task)
        except NoSuchElementException:
            logging.info("can not find the coin task icon")
            return False
        else:
            icon.click()
            try:
                self.driver.find_element(*self.viewTab)
            except NoSuchElementException:
                logging.info("you are not open coin task window")
                return False
            else:
                logging.info("now you are open coin task winodw")
                return True

    def switch_coin_task(self,task):
        '''选择coin task页面Tab'''
        logging.info("====switch coin task Tab====")
        if task == "Daily task":
            try:
                DailyTask = self.driver.find_element(*self.DailyTask)
            except NoSuchElementException:
                logging.info("can not find Daily Task Tab")
                return False
            else:
                if DailyTask.get_attribute("clickable") == "true":
                    DailyTask.click()
                    logging.info("now you swtich to Daily Task Tab")
                    return True
                else:
                    logging.info("you are already in Daily Task Page")
                    return True
        elif task == "Improvement task":
            try:
                ImprovementTask = self.driver.find_element(*self.ImprovementTask)
            except NoSuchElementException:
                logging.info("can not find Improvement Task Tab")
                return False
            else:
                if ImprovementTask.get_attribute("clickable") == "true":
                    ImprovementTask.click()
                    logging.info("now you switch to Improvement Task")
                    return True
                else:
                    logging.info("you are already in Improvement Task Page")
                    return True
        else:
            logging.info("coin task don't have this page")
            return False


    def check_wealth_rank(self):
        logging.info("=====check wealth right or wrong=====")
        try:
            wealth = self.driver.find_element(*self.wealth)
        except NoSuchElementException:
            logging.info("can not find wealth ,you are not in wealth rank page")
            return False
        else:
            if wealth.get_attribute("clickable") == "false":
                logging.info("you are in wealth rank page")
                return True
            else:
                logging.info("you are not in wealth rank page")
                return False

    def check_rooms_rank(self):
        logging.info("=====check rooms right or wrong=====")
        try:
            rooms = self.driver.find_element(*self.rooms)
        except NoSuchElementException:
            logging.info("can not find rooms ,you are not in rooms rank page")
            return False
        else:
            if rooms.get_attribute("clickable") == "false":
                logging.info("you are in rooms rank page")
                return True
            else:
                logging.info("you are not in rooms rank page")
                return False

    def check_charm_rank(self):
        logging.info("=====check charm right or wrong=====")
        try:
            charm = self.driver.find_element(*self.charm)
        except NoSuchElementException:
            logging.info("can not find charm ,you are not in charm rank page")
            return False
        else:
            if charm.get_attribute("clickable") == "false":
                logging.info("you are in charm rank page")
                return True
            else:
                logging.info("you are not in charm rank page")
                return False

    def check_gift_rank(self):
        logging.info("=====check gift right or wrong=====")
        try:
            gift = self.driver.find_element(*self.gift)
        except NoSuchElementException:
            logging.info("can not find gift ,you are not in gift rank page")
            return False
        else:
            if gift.get_attribute("clickable") == "false":
                logging.info("you are in gift rank page")
                return True
            else:
                logging.info("you are not in gift rank page")
                return False

    def go_anyroom(self,i):
        logging.info("====go to the " + str(i) + "room====")
        try:
            room = self.driver.find_elements(*self.room_icon)[i]
        except NoSuchElementException:
            logging.info("can not find the "+str(i)+" room")
            return False
        else:
            room_name = (self.driver.find_elements(*self.room_name)[i]).get_attribute("text")
            # print(room_name)
            room.click()
            if (self.driver.find_element(*self.room_inner_name)).get_attribute("text") == room_name:
                logging.info("you go to "+str(i)+" room success")
                return True
            else:
                logging.info("you go to the room failed")
                return False

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = HomeHotView(appium_desired(yaml_address))
    l.go_hot()
    # l.go_PK()
    # l.open_coin_task()
    # l.switch_coin_task("Improvement task")
    # l.check_banner()
    # l.go_anyroom(0)
    # l.go_CP_ranking()
    # l.go_Activity()
    l.go_ranking()
    # l.check_wealth_rank()
    # l.check_rooms_rank()
    # l.check_charm_rank()
    # l.check_gift_rank()
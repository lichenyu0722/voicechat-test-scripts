from common.desired_caps import appium_desired
from baseView.baseView import BaseView
from selenium.common.exceptions import NoSuchElementException
import logging,time
from selenium.webdriver.common.by import By


class HomeView(BaseView):

    me_page_button = (By.ID,'com.test.voice.chat:id/button3')
    home_page_button = (By.ID,'com.test.voice.chat:id/button1')
    message_page_button = (By.ID,'com.test.voice.chat:id/button2')
    match_page_button = (By.ID,'com.test.voice.chat:id/tvMatch')
    moment_page_button = (By.ID,'com.test.voice.chat:id/tvMoment')
    me_page_sign = (By.ID,'com.test.voice.chat:id/me_header')
    message_page_sign = (By.ID,'com.test.voice.chat:id/btn_right')
    home_page_sign = (By.ID,'com.test.voice.chat:id/enter_room_bt')

    CoinTaskTreasure = (By.ID,'com.test.voice.chat:id/ivCoinTaskTreasure')
    related_tap = (By.ID,'com.test.voice.chat:id/home_inner_tab_tv')
    explore_header = (By.ID,'com.test.voice.chat:id/home_content_header_tv')
    to_room = (By.ID,'com.test.voice.chat:id/ivEnter')
    creativeRoom_button = (By.ID,'com.test.voice.chat:id/btnCreateRoom')
    unlike = (By.ID,'com.test.voice.chat:id/ivUnLike')
    moment = (By.ID,'com.test.voice.chat:id/clMoment')

    Toptap = (By.ID,'com.test.voice.chat:id/tvTitle')

    def go_Me(self):
        logging.info('====go to Me page====')
        try:
            me_page_button = self.driver.find_element(*self.me_page_button)
        except NoSuchElementException:
            logging.info('can not find the me_page button')
            return False
        else:
            me_page_button.click()
            try:
                self.driver.find_element(*self.me_page_sign)
            except NoSuchElementException:
                logging.info(' you are not in me_page ')
                return False
            else:
                logging.info(' go to me_page success')
                return True

    def go_Message(self):
        logging.info("====go to Message page====")
        try:
            message_page_button = self.driver.find_element(*self.message_page_button)
        except NoSuchElementException:
            logging.info("can not find the message_page button")
            return False
        else:
            message_page_button.click()
            try:
                self.driver.find_element(*self.message_page_sign)
            except NoSuchElementException:
                logging.info(" you are not in message_page ")
                return False
            else:
                logging.info(" go to message_page success ")
                return True

    def go_Home(self):
        logging.info("====go to Home page====")
        try:
            home_page_button = self.driver.find_element(*self.home_page_button)
        except NoSuchElementException:
            logging.info("can not find the home_page button")
            return False
        else:
            home_page_button.click()
            try:
                self.driver.find_element(*self.home_page_sign)
            except NoSuchElementException:
                logging.info(" you are not in home_page ")
                return False
            else:
                logging.info(" go to home_page success ")
                return True

    def go_Match(self):
        logging.info("=====go to Match page=====")
        try:
            match_page_button = self.driver.find_element(*self.match_page_button)
        except NoSuchElementException:
            logging.info("can not find the match buttom")
            return False
        else:
            match_page_button.click()
            try:
                self.driver.find_element(*self.unlike)
            except NoSuchElementException:
                logging.info("you are not in match page")
                return False
            else:
                logging.info("go to match_page success")
                return True

    def go_Moment(self):
        logging.info("=====go to Moment page=====")
        try:
            moment_page_button = self.driver.find_element(*self.moment_page_button)
        except NoSuchElementException:
            logging.info("can not find the moment page")
            return False
        else:
            moment_page_button.click()
            try:
                self.driver.find_element(*self.moment)
            except NoSuchElementException:
                logging.info("you are not in moment page")
                return False
            else:
                logging.info("go to moment page success")
                return True

    def go_hot(self):
        logging.info("=====go to Hot Page=====")
        try:
            hot = self.driver.find_elements(*self.Toptap)[1]
        except NoSuchElementException:
            logging.info("can not find hot button")
            return False
        else:
            hot.click()
            try:
                self.driver.find_element(*self.CoinTaskTreasure)
            except NoSuchElementException:
                logging.info("you are not in Hot Page")
                return False
            else:
                logging.info("you switch Hot Page success")
                return True

    def go_related(self):
        logging.info("=====go to Related Page=====")
        try:
            related = self.driver.find_elements(*self.Toptap)[0]
        except NoSuchElementException:
            logging.info("can not find related button")
            return False
        else:
            related.click()
            try:
                self.driver.find_element(*self.related_tap)
            except NoSuchElementException:
                logging.info("you are not in Related Page")
                return False
            else:
                logging.info("you switch Related Page success")
                return True

    def go_explore(self):
        logging.info("=====go to Explore Page=====")
        try:
            explore = self.driver.find_elements(*self.Toptap)[2]
        except NoSuchElementException:
            logging.info("can not find explore button")
            return False
        else:
            explore.click()
            logging.info("you switch Explore Page success")
            return True
            # try:
            #     country = self.driver.find_elements(*self.explore_header)[0]
            # except NoSuchElementException:
            #     logging.info("you are not in Explore Page")
            #     return False
            # else:
            #     logging.info("you switch Explore Page success")
            #     return True
            #     # if country.get_attribute("text") == "Precious Gift":
            #     #     logging.info("you switch Explore Page success")
            #     #     return True
            #     # else:
            #     #     logging.info("you are not in Explore Page")
            #     #     return False

    def go_room(self):
        '''房间创建'''
        logging.info("go room")
        try:
            to_room = self.driver.find_element(*self.to_room)
        except NoSuchElementException:
            logging.info("can not find to_room button")
            return False
        else:
            to_room.click()
            try:
                self.driver.find_element(*self.creativeRoom_button)
            except NoSuchElementException:
                logging.info("you already creative room")
            else:
                logging.info("you are in creative page")


# if __name__ == '__main__':

    # yaml_address = "../../config/desired.yaml"
    # driver = appium_desired(yaml_address)
    # l = HomeView(driver)
    # l.go_Me()
    # l.go_Message()
    # l.go_Home()
    # l.go_Match()
    # l.go_Moment()
    # l.go_explore()
    # l.go_related()
    # l.go_hot()

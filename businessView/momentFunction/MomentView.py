import time

from common.desired_caps import appium_desired
from businessView.homeFunction.HomeView import HomeView
from common.common_fun import Common
from selenium.common.exceptions import NoSuchElementException
import logging
from selenium.webdriver.common.by import By

class MomentView(HomeView,Common):

    moment_post = (By.ID,'com.test.voice.chat:id/ivNew')
    add_photo = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/rvContent"]/android.widget.FrameLayout[1]/android.widget.ImageView[1]')
    new = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabMoment"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]')
    following = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabMoment"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]')
    hot = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/tabMoment"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[3]')
    like = (By.ID,'com.test.voice.chat:id/ivLike')
    like_number = (By.ID,'com.test.voice.chat:id/tvLike')
    comment = (By.ID,'com.test.voice.chat:id/tvComment')
    # comment_content = (By.ID,'com.test.voice.chat:id/tvContent')
    more = (By.ID,'com.test.voice.chat:id/ivMore')
    follow = (By.ID,'com.test.voice.chat:id/tvFollow')
    report = (By.ID,'com.test.voice.chat:id/tvReport')
    cancel = (By.ID,'com.test.voice.chat:id/tvCancel')
    inputText = (By.ID,'com.test.voice.chat:id/etInputText')
    send_comment = (By.ID,'com.test.voice.chat:id/btnSend')
    report_content = (By.ID,'com.test.voice.chat:id/etContent')
    report_submit = (By.ID,'com.test.voice.chat:id/tvSubmit')

    def go_moment_post_page(self):
        logging.info("=====go moment post page=====")
        try:
            post = self.driver.find_element(*self.moment_post)
        except NoSuchElementException:
            logging.info("can not find the post moment button")
            return False
        else:
            logging.info("click to go moment post page")
            post.click()
            try:
                self.driver.find_element(*self.add_photo)
            except NoSuchElementException:
                logging.info("you are not in moment post page")
                return False
            else:
                logging.info("now you are in moment post page")
                return True

    def check_new_page(self):
        logging.info("=====check new page=====")
        try:
            new = self.driver.find_element(*self.new)
        except NoSuchElementException:
            logging.info("can not find the new title")
            return False
        else:
            if new.get_attribute("clickable") == "true":
                logging.info("you are not in moment-new page")
                return False
            elif new.get_attribute("clickable") == "false":
                logging.info("now you are in moment-new page")
                return True

    def check_following_page(self):
        logging.info("=====check following page=====")
        try:
            following = self.driver.find_element(*self.following)
        except NoSuchElementException:
            logging.info("can not find the following title")
            return False
        else:
            if following.get_attribute("clickable") == "true":
                logging.info("you are not in moment-following page")
                return False
            elif following.get_attribute("clickable") == "false":
                logging.info("now you are in moment-following page")
                return True

    def check_hot_page(self):
        logging.info("=====check hot page======")
        try:
            hot = self.driver.find_element(*self.hot)
        except NoSuchElementException:
            logging.info("can not find the hot title")
            return False
        else:
            if hot.get_attribute("clickable") == "true":
                logging.info("you are not in moment-hot page")
                return False
            elif hot.get_attribute("clickable") == "false":
                logging.info("now you are in moment-hot page")
                return True

    def switch_to_hot_page(self):
        logging.info("=====switch to hot page=====")
        try:
            hot = self.driver.find_element(*self.hot)
        except NoSuchElementException:
            logging.info("can not find the hot title")
            return False
        else:
            logging.info("switch to hot page")
            hot.click()
            return True

    def switch_to_new_page(self):
        logging.info("=====switch to new page=====")
        try:
            new = self.driver.find_element(*self.new)
        except NoSuchElementException:
            logging.info("can not find the new title")
            return False
        else:
            logging.info("switch to new page")
            new.click()
            return True


    def switch_to_following_page(self):
        logging.info("=====switch to following page=====")
        try:
            following = self.driver.find_element(*self.following)
        except NoSuchElementException:
            logging.info("can not find the following title")
            return False
        else:
            logging.info("switch to following page")
            following.click()
            return True

    def check_following_page_blank(self):
        logging.info("=====check following page is blank or not=====")
        try:
            self.driver.find_element(by=By.XPATH,value='//*[@text="No moment"]')
        except NoSuchElementException:
            logging.info("the following page isn't blank")
            return False
        else:
            try:
                self.driver.find_element(by=By.ID,value="com.test.voice.chat:id/clRoot")
            except NoSuchElementException:
                logging.info("the blank following page without recommend user")
                return False
            else:
                logging.info("the following page is blank")
                return True

    def like_moment(self,option):
        logging.info("=====like moment=====")
        try:
            like_number = self.driver.find_element(*self.like_number)
        except NoSuchElementException:
            logging.info("can not find the like number")
            return False
        else:
            number_before = int(like_number.get_attribute("text"))
            logging.info("the number is "+str(number_before)+" before like.")
            try:
                like = self.driver.find_element(*self.like)
            except NoSuchElementException:
                logging.info("can not find the like button")
                return False
            else:
                if option == "cancel":
                    logging.info("click to not like this moment")
                    like.click()
                    number_after = int(like_number.get_attribute("text"))
                    if number_before - number_after == 1:
                        logging.info("Cancel like this moment success")
                        return True
                    else:
                        logging.info("Cancel like this moment failed")
                        return False
                elif option == "like":
                    logging.info("click to like this moment")
                    like.click()
                    number_after = int(like_number.get_attribute("text"))
                    if number_after - number_before == 1:
                        logging.info("Like this moment success")
                        return True
                    else:
                        logging.info("Like this moment failed")
                        return False

    def comment_moment(self,content_text):
        logging.info("=====comment moment=====")
        try:
            comment = self.driver.find_element(*self.comment)
        except NoSuchElementException:
            logging.info("can not find the comment button")
            return False
        else:
            logging.info("click comment to go moment details page")
            comment_number_before = int(comment.get_attribute("text"))
            comment.click()
            try:
                inputText = self.driver.find_element(*self.inputText)
            except NoSuchElementException:
                logging.info("you are not in moment details page,can not pull the keyboard")
                return False
            else:
                logging.info("input text："+content_text)
                inputText.send_keys(content_text)
                try:
                    comment_send = self.driver.find_element(*self.send_comment)
                except NoSuchElementException:
                    logging.info("you are not in moment details page,can not pull the keyboard")
                    return False
                else:
                    logging.info("click to send comment")
                    comment_send.click()
                    return comment_number_before

    def check_comment_success(self,content_text,comment_number_before):
        logging.info("=====check comment success or fail=====")
        l.swipeDown(a1=500, a2=500, b1=1500, b2=40)
        try:
            after_comment = self.driver.find_element(*self.comment)
        except NoSuchElementException:
            logging.info("can not find the comment number")
            return False
        else:
            comment_number_after = int(after_comment.get_attribute("text"))
            try:
                self.driver.find_elements(by=By.XPATH,value='//*[@text="'+content_text+'"]')[0]
            except NoSuchElementException:
                logging.info("you comment content can not find")
                return False
            else:
                # print(str(comment_number_before))
                # print(str(comment_number_after))
                # print(((self.driver.find_elements(by=By.XPATH,value='//*[@text="'+content_text+'"]'))[0]).get_attribute("text"))
                # print(content_text)
                # if comment_number_after - comment_number_before == 1 and (self.driver.find_element(by=By.XPATH,value='//*[@text="'+content_text+'"]')).get_attribute("text") == content_text :
                if (self.driver.find_element(by=By.XPATH,value='//*[@text="'+content_text+'"]')).get_attribute("text") == content_text :
                    logging.info("comment success")
                    return True
                else:
                    logging.info("comment failed")
                    return False

    def refresh_moment(self):
        logging.info("=====refresh moment=====")
        logging.info("swipe down to refresh moment")
        l.swipeDown(a1=500,a2=500,b1=500,b2=1500)
        logging.info("refresh moment success")

    def moment_more_option(self):
        logging.info("=====moment more options=====")
        try:
            more = self.driver.find_element(*self.more)
        except NoSuchElementException:
            logging.info("can not find the more option")
            return False
        else:
            logging.info("more option click")
            more.click()
            try:
                self.driver.find_element(*self.report)
            except NoSuchElementException:
                logging.info("more option open fail")
                return False
            else:
                logging.info("more option open success")
                return True

    def report_moment(self,content):
        logging.info("=====report moment======")
        try:
            report = self.driver.find_element(*self.report)
        except NoSuchElementException:
            logging.info("can not find the report button")
            return False
        else:
            logging.info("click to report moment")
            report.click()
            try:
                report_content = self.driver.find_element(*self.report_content)
            except NoSuchElementException:
                logging.info("can not find the report content")
                return False
            else:
                logging.info("report submit content: "+content)
                report_content.send_keys(content)
                try:
                    submit = self.driver.find_element(*self.report_submit)
                except NoSuchElementException:
                    logging.info("can not find the submit button")
                    return False
                else:
                    logging.info("click to submit report")
                    submit.click()
                    return True

    def follow_user_post_moment(self):
        logging.info("=====follow user post moment=====")
        try:
            follow = self.driver.find_element(*self.follow)
        except NoSuchElementException:
            logging.info("can not find the follow button")
            return False
        else:
            if follow.get_attribute("text") == "Follow":
                logging.info("follow user success")
                follow.click()
                return True
            elif follow.get_attribute("text") == "Unfollow":
                logging.info("you already follow the user")
                return True
            else:
                logging.info("something wrong")
                return False

    def unfollow_user_post_moment(self):
        logging.info("=====unfollow user post moment=====")
        try:
            unfollow = self.driver.find_element(*self.follow)
        except NoSuchElementException:
            logging.info("can not find the unfollow button")
            return False
        else:
            if unfollow.get_attribute("text") == "Unfollow":
                unfollow.click()
                logging.info("unfollow user success")
                return True
            elif unfollow.get_attribute("text") == "Follow":
                logging.info("you are not follow the user")
                return False
            else:
                logging.info("something wrong")
                return False

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = MomentView(driver)
    l.go_Moment()
    l.switch_to_following_page()
    l.moment_more_option()
    l.unfollow_user_post_moment()
    l.refresh_moment()
    # l.report_moment(content="For test")
    # l.follow_user_post_moment()
    # l.go_moment_post_page()
    # l.switch_to_following_page()
    # '''向下滑动态'''
    # logging.info("向下滑动态")
    # l.swipeDown(a1=500, a2=500, b1=1500, b2=40)
    # '''向上滑动态'''
    # logging.info("向上滑动态")
    # l.swipeDown(a1=500, a2=500, b1=500, b2=1500)
    # l.check_following_page_blank()
    # l.like_moment(option="like")
    # before = l.comment_moment(content_text="真的🐂🍺")
    # l.check_comment_success(content_text="真的🐂🍺",comment_number_before=before)
    # l.refresh_moment()
from common.desired_caps import appium_desired
from businessView.momentFunction.MomentPostView import MomentPostView
from selenium.common.exceptions import NoSuchElementException
import logging,random,time
from selenium.webdriver.common.by import By

class MomentPhotoSelectView(MomentPostView):

    photo = (By.ID,'com.test.voice.chat:id/tvIndex')
    sure = (By.ID,'com.test.voice.chat:id/tvSure')
    camera = (By.ID,'com.test.voice.chat:id/ivCamera')
    add_tag = (By.ID,'com.test.voice.chat:id/tvAddTag')
    tag = (By.ID,'com.test.voice.chat:id/tvTag')
    delete_tag = (By.ID,'com.test.voice.chat:id/ivDeleteTag')

    def select_photo(self,i):
        logging.info("=====select photo=====")
        try:
            photo = self.driver.find_elements(*self.photo)[i]
        except NoSuchElementException:
            logging.info("can not find the photo")
            return False
        else:
            logging.info("click to select photo")
            photo.click()
            return True

    def photo_sure(self):
        logging.info("=====sure button click====")
        try:
            sure = self.driver.find_element(*self.sure)
        except NoSuchElementException:
            logging.info("can not find the sure")
            return False
        else:
            logging.info("click to sure")
            sure.click()
            return True

    def take_photo(self):
        logging.info("take photo")

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = MomentPhotoSelectView(driver)
    for i in range(1,10):
        l.go_Moment()
        l.go_moment_post_page()
        l.go_select_photo_page()
        ran_number = random.randint(1, 10)
        l.select_photo(ran_number)
        l.photo_sure()
        l.input_content_text(str="Test Moment "+str(i))
        l.post_moment()
        print(i)
        time.sleep(5)
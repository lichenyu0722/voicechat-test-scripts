from common.desired_caps import appium_desired
from businessView.momentFunction.MomentView import MomentView
from selenium.common.exceptions import NoSuchElementException
import logging,random
from selenium.webdriver.common.by import By

class MomentPostView(MomentView):

    add_photo = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/rvContent"]'
                          '/android.widget.FrameLayout[1]/android.widget.ImageView[1]')
    post = (By.ID,'com.test.voice.chat:id/tvSend')
    content_text = (By.ID,'com.test.voice.chat:id/etContent')
    camera = (By.ID,'com.test.voice.chat:id/ivCamera')
    photo_permission = (By.ID, 'com.android.packageinstaller:id/permission_allow_button')
    add_tag = (By.ID,'com.test.voice.chat:id/tvAddTag')
    confirm = (By.ID,'com.test.voice.chat:id/tvConfirm')
    # tag = (By.XPATH,'//*[@text='+tag+']')
    tag = (By.ID,'com.test.voice.chat:id/tvTag')
    delete_tag = (By.ID,'com.test.voice.chat:id/ivDeleteTag')


    def go_select_photo_page(self):
        logging.info("====select photo=====")
        try:
            add_photo = self.driver.find_element(*self.add_photo)
        except NoSuchElementException:
            logging.info("can not find the add_photo button")
            return False
        else:
            add_photo.click()
            try:
                allow = self.driver.find_element(*self.photo_permission)
            except NoSuchElementException:
                logging.info("you are already get photo permission")
                pass
            else:
                logging.info("get photo permission")
                allow.lick()
                return True


    def post_moment(self):
        logging.info("=====post moment=====")
        try:
            post =self.driver.find_element(*self.post)
        except NoSuchElementException:
            logging.info("can not find the post button")
            return False
        else:
            logging.info("click to post moment")
            post.click()
            return True

    def input_content_text(self,str):
        logging.info("=====input content text=====")
        try:
            text = self.driver.find_element(*self.content_text)
        except NoSuchElementException:
            logging.info("can not find the content text")
            return False
        else:
            logging.info("clear content text")
            text.clear()
            logging.info("input content text: "+str)
            text.send_keys(str)
            return True

    def open_camera(self):
        logging.info("=====open the phone camera=====")
        try:
            camera = self.driver.find_element(*self.camera)
        except NoSuchElementException:
            logging.info("can not find the camera")
            return False
        else:
            camera.click()
            try:
                allow = self.driver.find_element(*self.photo_permission)
            except NoSuchElementException:
                logging.info("you are already photo permission")
                pass
            else:
                logging.info('get photo permission')
                allow.click()
                return True

    def add_Tag(self):
        logging.info("======add moment tag======")
        try:
            add_tag = self.driver.find_element(*self.add_tag)
        except NoSuchElementException:
            logging.info("can not find the add_tag button")
            return False
        else:
            logging.info("click to add moment tag")
            add_tag.click()
            try:
                self.driver.find_element(*self.confirm)
            except NoSuchElementException:
                logging.info("you are not in tag select page")
                return False
            else:
                logging.info("now you are in tag select page")
                return True

    def select_tag(self,tag_content):
        logging.info("=====select tag：%s=====",tag_content)
        try:
            tag = self.driver.find_element(by=By.XPATH,value='//*[@text='+'"'+tag_content+'"'+']')
        except NoSuchElementException:
            logging.info("can not find the tag")
            return False
        else:
            logging.info("select tag")
            tag.click()
            try:
                confirm  = self.driver.find_element(*self.confirm)
            except NoSuchElementException:
                logging.info("can not find the Add Tag button")
                return False
            else:
                logging.info("click to confirm tag")
                confirm.click()
                try:
                    selected_tag = self.driver.find_element(*self.tag)
                except NoSuchElementException:
                    logging.info("select tag failed")
                    return False
                else:
                    if selected_tag.get_attribute("text") != tag_content:
                        logging.info("select tag wrong")
                        print("selected_tag："+selected_tag.get_attribute("text"))
                        print("input tag："+tag_content)
                        return False
                    elif selected_tag.get_attribute("text") == tag_content:
                        logging.info("select tag success")
                        return True
                    else:
                        logging.info("something wrong")
                        return False




if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = MomentPostView(driver)
    l.go_Moment()
    l.go_moment_post_page()
    l.add_Tag()
    l.select_tag(tag_content="生日")
    # l.go_select_photo_page()
    # ran_number = random.randint(1, 15)


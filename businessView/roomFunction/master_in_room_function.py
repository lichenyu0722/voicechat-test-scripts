from businessView.homeFunction.HomeCreativeRoomView import HomeCreativeRoom
from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging,time,os

class MasterInRoom(HomeCreativeRoom):

    more_btn = (By.ID,'com.test.voice.chat:id/btnMore')
    exit_more_option = (By.ID,'com.test.voice.chat:id/ivMoreOption')
    keep = (By.ID,'com.test.voice.chat:id/tvKeep')
    exit = (By.ID,'com.test.voice.chat:id/tvExit')
    RoomHead = (By.ID,'com.test.voice.chat:id/ivRoomHead')
    Float = (By.ID,'com.test.voice.chat:id/ivFloat')

    RoomName = (By.ID,'com.test.voice.chat:id/tvRoomName')
    RoomContribution = (By.ID,'com.test.voice.chat:id/tvMonthRankValue')

    title = (By.ID,'com.test.voice.chat:id/tvTitle')
    userTotalAccount = (By.ID,'com.test.voice.chat:id/tvUserTotalAccount')
    userInfo = (By.ID,'com.test.voice.chat:id/llUInfo')
    btn_gift = (By.ID,'com.test.voice.chat:id/btn_gift')
    gift_all = (By.ID,'com.test.voice.chat:id/tvAll')

    input_mode = (By.ID,'com.test.voice.chat:id/btn_input_mode')
    input_text = (By.ID,'com.test.voice.chat:id/et_input_text')

    btn_send = (By.ID,'com.test.voice.chat:id/btn_send')
    mic_Bg = (By.ID,'com.test.voice.chat:id/ivMicBg')

    tvCount = (By.ID,'com.test.voice.chat:id/tvCount')

    btn_sound = (By.ID,'com.test.voice.chat:id/btn_sound')

    tv_wheel = (By.ID,'com.test.voice.chat:id/ivWheel')
    wheel_roll = (By.ID,'com.test.voice.chat:id/ivWheelNum')
    wheel_go = (By.ID,'com.test.voice.chat:id/ivGo')
    wheel_result = (By.ID,'com.test.voice.chat:id/llResult')
    result_number = (By.ID,'com.test.voice.chat:id/tvResult')
    wheel_close = (By.ID,'com.test.voice.chat:id/ivClose')

    room_share = (By.ID,'com.test.voice.chat:id/ivShare')
    share_title = (By.ID,'com.test.voice.chat:id/tv_title')

    userhead_mic = (By.ID,'com.test.voice.chat:id/ivItemUserHead')
    btn_mic = (By.ID,'com.test.voice.chat:id/btn_mic')

    more_option_content = (By.ID,'com.test.voice.chat:id/tvContent')
    room_egg = (By.ID,'com.test.voice.chat:id/ivRoomEgg')
    hammer_1 = (By.ID,'com.test.voice.chat:id/bgHammer1')
    hammer_10 = (By.ID,'com.test.voice.chat:id/bgHammer10')
    hammer_100 = (By.ID,'com.test.voice.chat:id/bgHammer100')
    smash_agan = (By.ID,'com.test.voice.chat:id/tvSubmit')

    def check_my_room(self):
        '''检测是否为自己的房间'''
        logging.info("check my room")

        try:
            self.driver.find_element(*self.more_btn)
        except NoSuchElementException:
            logging.info("it's not your room")
            return False
        else:
            logging.info("it's my room")
            return True

    def exit_room(self):
        '''退出房间'''
        logging.info("====exit room====")
        try:
            exit_more_option = self.driver.find_element(*self.exit_more_option)
        except NoSuchElementException:
            logging.info("can not find the exit more option")
            return False
        else:
            exit_more_option.click()
            logging.info("click exit")
            try:
                exit = self.driver.find_element(*self.exit)
            except NoSuchElementException:
                logging.info("can not find the exit")
                return False
            else:
                exit.click()
                logging.info("check exit room success or fail")
                try:
                    self.driver.find_element(*self.RoomHead)
                except NoSuchElementException:
                    logging.info("exit room success")
                    return True
                else:
                    logging.info("exit room failed,you are still in teh room")
                    return False

    def room_share_open(self):
        '''房间分享'''
        logging.info("====room share====")
        try:
            share = self.driver.find_element(*self.room_share)
        except NoSuchElementException:
            logging.info("can not find the room share button")
            return False
        else:
            share.click()
            try:
                share_title = self.driver.find_element(*self.share_title)
            except NoSuchElementException:
                logging.info("can not open share page")
                return False
            else:
                if share_title.get_attribute("text") == "Share SoulFa To":
                    logging.info("open share page success")
                    os.system("adb shell input keyevent 4")
                    return True
                else:
                    logging.info("open share page failed")
                    return False


    def keep_room(self):
        '''keep缩小房间'''
        logging.info("====keep room====")
        try:
            exit_more_option = self.driver.find_element(*self.exit_more_option)
        except NoSuchElementException:
            logging.info("can not find the exit more option")
            return False
        else:
            exit_more_option.click()
            logging.info("click keep")
            try:
                keep = self.driver.find_element(*self.keep)
            except NoSuchElementException:
                logging.info("can not find the keep")
                return False
            else:
                keep.click()
                logging.info("check keep room success or fail")
                try:
                    self.driver.find_element(*self.Float)
                except NoSuchElementException:
                    logging.info("can not found Float ,keep failed")
                    return False
                else:
                    logging.info("keep success")
                    return True

    def back_to_room(self):
        '''keep返回房间'''
        logging.info("====back to room====")
        try:
            float = self.driver.find_element(*self.Float)
        except NoSuchElementException:
            logging.info("can not found Float ,keep failed")
            return False
        else:
            float.click()
            logging.info("check back to room success or fail")
            try:
                self.driver.find_element(*self.RoomHead)
            except NoSuchElementException:
                logging.info("back to room failed")
                return False
            else:
                logging.info("back to room success")
                return True

    def more_option(self):
        '''房主更多功能操作'''
        logging.info("====more option====")
        try:
            more = self.driver.find_element(*self.more_btn)
        except NoSuchElementException:
            logging.info("can not find the more btn")
            return False
        else:
            more.click()
            return True

    def go_cover_info(self):
        '''进入cover_info修改房间信息'''
        logging.info("====go to cover_info====")
        try:
            cover_info = self.driver.find_elements(*self.more_option_content)[1]
        except NoSuchElementException:
            logging.info("can not find the cover_info")
            return False
        else:
            if cover_info.get_attribute("text") == "Cover Info":
                cover_info.click()
                logging.info("go to edit room information")
                try:
                    self.driver.find_element(*self.btnCreateRoom)
                except NoSuchElementException:
                    logging.info("can not find the btnCreativeRoom")
                    return False
                else:
                    logging.info("you already in cover_info")
                    return True
            else:
                logging.info("can not find the cover_info")
                return False

    def go_music(self,mic_position,option):
        '''进入房间音乐'''
        logging.info("====go to music====")
        try:
            music = self.driver.find_elements(*self.more_option_content)[2]
        except NoSuchElementException:
            logging.info("can not find the music")
            return False
        else:
            if music.get_attribute("text") == "Music":
                logging.info("go to room musice page")
                music.click()
                toast_text = self.get_toast_text(text="You can upload music after taking the mic before")
                print(toast_text)
                if toast_text == "You can upload music after taking the mic before":
                    logging.info("you are not in the mic,so you can't open music")
                    '''执行上麦操作'''
                    self.blank_mic_option(mic_position=mic_position,option=option)
                    time.sleep(2)
                    self.more_option()
                    time.sleep(2)
                    try:
                        music = self.driver.find_elements(*self.more_option_content)[2]
                    except NoSuchElementException:
                        logging.info("can not find the music")
                        return False
                    else:
                        if music.get_attribute("text") == "Music":
                            logging.info("go to room musice page")
                            music.click()
                    try:
                        self.driver.find_element(*self.title)
                    except NoSuchElementException:
                        logging.info("you are not in music page")
                        return False
                    else:
                        logging.info("now you are in music page")
                        return True
                else:
                    try:
                        self.driver.find_element(*self.title)
                    except NoSuchElementException:
                        logging.info("you are not in music page")
                        return False
                    else:
                        logging.info("now you are in music page")
                        return True

    # def sound_close(self):
    #     '''声音开关操作'''
    #     logging.info("====close sound=====")
    #     try:
    #         sound = self.driver.find_element(*self.btn_sound)
    #     except NoSuchElementException:
    #         logging.info("can not find the sound button")
    #         return False
    #     else:
    #         sound.click()

    def open_wheel(self,mic_position,option):
        '''打开转盘'''
        logging.info("=====open wheel====")
        try:
            wheel = self.driver.find_element(*self.tv_wheel)
        except NoSuchElementException:
            logging.info("can not find the wheel")
            return False
        else:
            wheel.click()
            toast_text = self.get_toast_text(text="Start the game after taking the mic")
            print(toast_text)
            if toast_text == "Start the game after taking the mic":
                logging.info("you are not in the mic,so you can't open wheel")
                #执行上麦操作
                self.blank_mic_option(mic_position=mic_position,option=option)
                time.sleep(5)
                wheel.click()
                try:
                    self.driver.find_element(*self.wheel_roll)
                except NoSuchElementException:
                    logging.info("can not find the wheel roll")
                    return False
                else:
                    logging.info("now you are open wheel")
                    return True
            else:
                try:
                    self.driver.find_element(*self.wheel_roll)
                except NoSuchElementException:
                    logging.info("can not find the wheel roll")
                    return False
                else:
                    logging.info("now you are open wheel")
                    return True

    def roll_wheel(self):
        '''转动转盘'''
        logging.info("=====roll the wheel=====")
        try:
            go = self.driver.find_element(*self.wheel_go)
        except NoSuchElementException:
            logging.info("can not find the go button")
            return False
        else:
            go.click()
            time.sleep(3)
            try:
                self.driver.find_element(*self.wheel_result)
            except NoSuchElementException:
                logging.info("can not find the wheel result")
                return False
            else:
                logging.info("roll wheel success")
                return True

    def get_wheel_result(self):
        '''获取转盘结果'''
        logging.info("=====get wheel result=====")
        try:
            wheel_result = self.driver.find_element(*self.result_number)
        except NoSuchElementException:
            logging.info("can not get wheel result number")
            return False
        else:
            result = wheel_result.get_attribute("text")
            print(result)
            return True

    def close_wheel(self):
        '''关闭转盘'''
        logging.info("====close wheel====")
        try:
            wheel_close = self.driver.find_element(*self.wheel_close)
        except NoSuchElementException:
            return False
        else:
            wheel_close.click()
            time.sleep(1)
            try:
                self.driver.find_element(*self.more_btn)
            except NoSuchElementException:
                logging.info("the wheel isn't close")
                return False
            else:
                logging.info("the wheel close success")
                return True

    def view_room_info(self):
        '''查看房间信息'''
        logging.info("====view room info====")
        try:
            RoomHead = self.driver.find_element(*self.RoomHead)
        except NoSuchElementException:
            logging.info("can not find the RoomHead")
            return False
        else:
            RoomHead.click()
            logging.info("check room info card")
            try:
                self.driver.find_element(*self.RoomName)
            except NoSuchElementException:
                logging.info("can not find room info card")
                return False
            else:
                logging.info("now you see room info card")
                return True

    def view_room_contribution(self):
        '''查看房间排行榜'''
        logging.info("====view room contribution====")
        try:
            room_contribution = self.driver.find_element(*self.RoomContribution)
        except NoSuchElementException:
            logging.info("can not find the room_contribution")
            return False
        else:
            room_contribution.click()
            logging.info("check room contribution")
            try:
                contribution_name = self.driver.find_element(*self.title)
            except NoSuchElementException:
                logging.info("open room contribution failed")
                return False
            else:
                if contribution_name.get_attribute("text") == "Contribution":
                    logging.info("now you see room contribution")
                    return True
                else:
                    logging.info("open room contribution failed")
                    return False

    def view_gift(self):
        '''打开礼物面板'''
        logging.info("====view gift====")
        try:
            gift = self.driver.find_element(*self.btn_gift)
        except NoSuchElementException:
            logging.info("can not find the gift")
            return False
        else:
            gift.click()
            logging.info("check gift page")
            try:
                self.driver.find_element(*self.gift_all)
            except NoSuchElementException:
                logging.info("open gift page failed")
                return False
            else:
                logging.info("now you are in gift page")
                return True

    def view_User_account(self):
        '''打开用户列表'''
        logging.info("====view user account====")
        try:
            userAccountInfo = self.driver.find_element(*self.userTotalAccount)
        except NoSuchElementException:
            logging.info("can not find the User account")
            return False
        else:
            userAccountInfo.click()
            try:
                self.driver.find_element(*self.userInfo)
            except NoSuchElementException:
                logging.info("open Online User failed")
                return False
            else:
                logging.info("now you are in Online User Page")
                return True

    def room_chat_input(self,message):
        '''聊天框输入信息'''
        logging.info("====now input "+message+" in your room chat====")
        try:
            input_box = self.driver.find_element(*self.input_mode)
        except NoSuchElementException:
            logging.info("can not find room input box")
            return False
        else:
            input_box.click()
            try:
                input_text = self.driver.find_element(*self.input_text)
            except NoSuchElementException:
                logging.info("can not find input text")
                return False
            else:
                input_text.clear()
                input_text.send_keys(message)
                return True

    def room_send_message(self):
        '''消息发送'''
        logging.info("====send message====")
        try:
            send = self.driver.find_element(*self.btn_send)
        except NoSuchElementException:
            logging.info("can not find the send button")
            return False
        else:
            send.click()
            return True

    def blank_mic_option(self,mic_position,option):
        '''房主上麦
        mic_position=0---1号麦
        mic_position=1---2号麦
        mic_position=2---3号麦
        mic_position=3---4号麦
        mic_position=4---5号麦
        mic_position=5---6号麦
        mic_position=6---7号麦
        mic_position=7---8号麦
        mic_position=8---9号麦
        mic_position=9---10号麦

        option=0---Take the Mic
        option=1---Give the Mic
        option=2---Lock the Mic
        option=3---Mute the Mic
        option=4---Cancel
        '''
        logging.info("====blank mic option=====")
        if option == 0:
            logging.info("execute Take the mic option")
        elif option == 1:
            logging.info("execute Give the mic option")
        elif option == 2:
            logging.info("execute Lock the mic option")
        elif option ==3:
            logging.info("execute Mute the mic option")
        else:
            logging.info("cancel")
        try:
            mic = self.driver.find_elements(*self.mic_Bg)[mic_position]
        except NoSuchElementException:
            logging.info("can not find the mic")
            return False
        else:
            mic.click()
            #差一个上麦权限获取逻辑
            try:
                count = self.driver.find_elements(*self.tvCount)[option]
            except NoSuchElementException:
                logging.info("can not find the function count")
                return False
            else:
                count.click()
                return True

    def check_on_mic(self,mic_position):
        '''检查用户是否已经处于麦上'''
        logging.info("====check user on mic====")
        try:
            self.driver.find_element(*self.btn_mic)
        except NoSuchElementException:
            logging.info("can not find the mic button,the user is not on mic")
            return False
        else:
            try:
                self.driver.find_elements(*self.userhead_mic)[mic_position]
            except NoSuchElementException:
                logging.info("can not find the user head on mic")
                return False
            else:
                logging.info("The user on mic now")
                return True


    def open_smash_egg(self):
        '''打开砸蛋功能'''
        logging.info("=====open smash egg=====")
        try:
            room_egg = self.driver.find_element(*self.room_egg)
        except NoSuchElementException:
            logging.info("can not find the room_egg")
            return False
        else:
            logging.info("open smash egg")
            room_egg.click()
            return True

    def hammer_egg_100(self):
        '''砸蛋'''
        logging.info("=====hammer egg=====")
        try:
            hammer = self.driver.find_element(*self.hammer_100)
        except NoSuchElementException:
            logging.info("can not find the hammer")
            return False
        else:
            hammer.click()
            return True

    def smash_again(self):
        '''再砸一次'''
        logging.info("=====smash again======")
        try:
            smash_again = self.driver.find_element(*self.smash_agan)
        except NoSuchElementException:
            logging.info("smash again")
            return False
        else:
            smash_again.click()
            return True

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = MasterInRoom(appium_desired(yaml_address=yaml_address))
    # print(time.time())
    l.go_room()
    l.open_smash_egg()
    l.hammer_egg_100()
    for i in range(0,99):
        l.smash_again()
        print(i)
    # l.go_on_mic(i=0,j=0)
    # mic_position = 1
    # option = 0
    # # l.blank_mic_option(mic_position=mic_position,option=option)
    # l.more_option()
    # l.go_music(mic_position=mic_position,option=option)
    # l.open_wheel(mic_position=mic_position,option=option)
    # l.roll_wheel()
    # l.get_wheel_result()
    # l.close_wheel()
    # l.room_share_open()
    # l.check_on_mic(mic_position=mic_position)
    # l.exit_room()
    # l.room_chat_input(message="这是一个奇怪的故事")
    # l.room_send_message()
    # print(time.asctime(time.localtime(time.time())))
    # l.check_my_room()
    # l.keep_room()
    # l.go_Me()
    # l.back_to_room()
    # l.more_option()
    # l.go_cover_info()
    # l.select_room_avator()
    # l.input_room_name(RoomName="this is my room")
    # l.creative_room_sure()
    # print(time.time())
    # l.view_room_info()
    # l.back()
    # l.view_gift()
    # l.back()
    # l.view_room_contribution()
    # l.back()
    # l.view_User_account()
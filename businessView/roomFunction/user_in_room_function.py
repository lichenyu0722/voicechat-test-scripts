from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging,time,os
from businessView.homeFunction.HomeView import HomeView

class UserInRoom(HomeView):

    followBtn = (By.ID,'com.test.voice.chat:id/tvFollowStatus')

    def follow_room(self):
        logging.info("=====follow the room=====")
        try:
            follow = self.driver.find_element(*self.followBtn)
        except NoSuchElementException:
            logging.info("can not find the follow button")
            return False
        else:
            follow.click()
            return True

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = UserInRoom(appium_desired(yaml_address=yaml_address))
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging,os
from common.desired_caps import appium_desired
from common.common_fun import Common

class SearchPageView(Common):

    searchBtn = (By.ID,'com.test.voice.chat:id/search_bt')
    search_edit = (By.ID,'com.test.voice.chat:id/edit_search')

    def go_search_Page(self):
        logging.info("====go search page====")
        try:
            search = self.driver.find_element(*self.searchBtn)
        except NoSuchElementException:
            logging.info("can not find the search button")
            return False
        else:
            logging.info("click to search page")
            search.click()
            try:
                self.driver.find_element(*self.search_edit)
            except NoSuchElementException:
                logging.info("you are not in search page")
                return False
            else:
                logging.info("now you are in search page")
                return True


    def search_input(self,show_id):
        logging.info("input show_id that you want to search")
        try:
            search_input = self.driver.find_element(*self.search_edit)
        except NoSuchElementException:
            logging.info("can not find the search edit")
            return False
        else:
            logging.info("input search show_id")
            #查看手机可用的软键盘
            os.system("adb shell ime list -s")
            #调起手机软键盘
            os.system("adb shell ime set com.sohu.inputmethod.sogou.xiaomi/.SogouIME")
            search_input.send_keys(show_id)
            if search_input.get_attribute("text") == show_id:
                logging.info("you input success"+", input id: "+show_id)
                #点击键盘中的搜索按键
                os.system("adb shell input keyevent 66")
                return True
            else:
                logging.info("you input failed")
                return False


if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = SearchPageView(appium_desired(yaml_address))
    l.go_search_Page()
    l.search_input(show_id="2423012")
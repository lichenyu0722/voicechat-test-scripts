from businessView.homeFunction.HomeHotView import HomeHotView
from common.desired_caps import appium_desired
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import logging


class RankAll(HomeHotView):

    day = (By.XPATH,'//*[@content-desc="24 hours"]')
    weekly = (By.XPATH,'//*[@content-desc="Weekly"]')
    monthly = (By.XPATH,'//*[@content-desc="Monthly"]')
    my_username = (By.XPATH,'//*[@resource-id="com.test.voice.chat:id/clBottom"]/android.widget.TextView[1]')
    username = (By.ID,'com.test.voice.chat:id/tvName')

    top_1 = (By.ID,'com.test.voice.chat:id/tvNameNo1')
    top_2 = (By.ID,'com.test.voice.chat:id/tvNameNo2')
    top_3 = (By.ID,'com.test.voice.chat:id/tvNameNo3')

    def switch_to_charm(self):
        '''切换到charm'''
        logging.info("====switch to charm====")
        try:
            charm = self.driver.find_element(*self.charm)
        except NoSuchElementException:
            logging.info("can not find the charm")
            return False
        else:
            charm.click()
            if charm.get_attribute("clickable") == "false":
                logging.info("now you are in charm page")
                return True
            else:
                logging.info("you are not in charm page")
                return False

    def switch_to_wealth(self):
        '''切换到charm'''
        logging.info("====switch to wealth====")
        try:
            wealth = self.driver.find_element(*self.wealth)
        except NoSuchElementException:
            logging.info("can not find the wealth")
            return False
        else:
            wealth.click()
            if wealth.get_attribute("clickable") == "false":
                logging.info("now you are in wealth page")
                return True
            else:
                logging.info("you are not in wealth page")
                return False

    def switch_24hours(self):
        '''切换至24hours'''
        logging.info("====switch to 24hours====")
        try:
            day = self.driver.find_element(self.day)
        except NoSuchElementException:
            logging.info("can not find the day(24hours)")
            return False
        else:
            day.click()
            if day.get_attribute("clickable") == "false":
                logging.info("now you are in 24hours page")
                return True
            else:
                logging.info("you are not in 24hours page")
                return False

    def switch_weekly(self):
        '''切换到weekly'''
        logging.info("====switch to weekly====")
        try:
            weekly = self.driver.find_element(*self.weekly)
        except NoSuchElementException:
            logging.info("can noy find the weekly")
            return False
        else:
            weekly.click()
            if weekly.get_attribute("clickable") == "false":
                logging.info("now you are in weekly page")
                return True
            else:
                logging.info("you are not in weekly page")
                return False

    def switch_monthly(self):
        '''切换到monthly'''
        logging.info("====switch to monthly====")
        try:
            monthly = self.driver.find_element(*self.monthly)
        except NoSuchElementException:
            logging.info("can not find the monthly")
            return False
        else:
            monthly.click()
            if monthly.get_attribute("clickable") == "false":
                logging.info("now you are in monthly page")
                return True
            else:
                logging.info("you are not in monthly page")
                return False

    def get_my_username(self):
        '''获取我的用户名'''
        logging.info("====get my username====")
        try:
            my_name = self.driver.find_element(*self.my_username)
        except NoSuchElementException:
            logging.info("can not find my username")
            return False
        else:
            logging.info("get my username success")
            return my_name.get_attribute("text")

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    l = RankAll(appium_desired(yaml_address=yaml_address))
    l.go_hot()
    l.go_gift_received()
    # l.switch_24hours()
    # l.switch_weekly()
    # l.switch_monthly()
    # l.switch_to_wealth()
    # l.switch_24hours()
    # l.switch_weekly()
    # l.switch_monthly()
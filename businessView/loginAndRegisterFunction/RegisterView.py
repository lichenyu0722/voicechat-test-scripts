import random,string
from common.desired_caps import appium_desired
from businessView.loginAndRegisterFunction.LoginView import LoginView
from businessView.meFunction.meInformation.MeEditView import MeEditView
from common.common_fun import Common
from selenium.common.exceptions import NoSuchElementException
import logging,time
from selenium.webdriver.common.by import By

class RegisterView(LoginView,MeEditView,Common):

    username = (By.ID,'com.test.voice.chat:id/etName')
    age_select = (By.ID,'com.test.voice.chat:id/tvAgeContent')
    clear_username = (By.ID,'com.test.voice.chat:id/ibClearNameInput')

    male = (By.ID,'com.test.voice.chat:id/tvMale')
    female = (By.ID,'com.test.voice.chat:id/tvFemale')
    other = (By.ID,'com.test.voice.chat:id/tvOtherGender')

    user_photo = (By.ID,'com.test.voice.chat:id/sivPhoto')

    ok_button = (By.ID,'com.test.voice.chat:id/tv_finish')

    confirm = (By.ID,'com.test.voice.chat:id/tvConfirm')

    def first_header_edit(self):
        '''选择头像'''
        logging.info("====register header photo set====")
        try:
            header = self.driver.find_element(*self.user_photo)
        except NoSuchElementException:
            logging.info('can not find the header')
            return False
        else:
            header.click()
            self.photo_permission_allow()
            try:
                self.driver.find_element(*self.photo_page)
            except NoSuchElementException:
                logging.info("you are not in photo page")
                return False
            else:
                ran_number = random.randint(0,8)
                print(ran_number)
                try:
                    photo = self.driver.find_elements(*self.photo)[0]
                except NoSuchElementException:
                    logging.info('can not find the photo')
                    return False
                else:
                    photo.click()
                    try:
                        self.driver.find_element(*self.photo_yes)
                    except NoSuchElementException:
                        logging.info("click again")
                        photo.click()
                    else:
                        logging.info("photo cut")
                        try:
                            photo_yes = self.driver.find_element(*self.photo_yes)
                        except:
                            logging.info("can not find photo_yes button")
                            return False
                        else:
                            photo_yes.click()
                            logging.info("photo_yes button click,selected photo ready")
                            return True


    def username_input(self,username):
        '''输入用户名'''
        logging.info("====username input=====")
        try:
            username_input = self.driver.find_element(*self.username)
        except NoSuchElementException:
            logging.info("can not find username")
            return False
        else:
            self.username_clear()
            username_input.send_keys(username)
            if username_input.get_attribute("text") == username:
                logging.info('username input:' + username)
                return True
            else:
                logging.info('you input somethng wrong')
                return False

    def username_clear(self):
        '''清除用户名'''
        logging.info("====clear username====")
        try:
            clear = self.driver.find_element(*self.clear_username)
        except NoSuchElementException:
            logging.info("can not find the clear_username button")
            return False
        else:
            logging.info("click to clear username")
            clear.click()
            try:
                username_input = self.driver.find_element(*self.username)
            except NoSuchElementException:
                logging.info("can not find username")
                return False
            else:
                if username_input.get_attribute('text') == "Enter your name":
                    logging.info("you are clear username")
                    return True
                else:
                    logging.info("you are not clear username")
                    clear.click()
                    return True


    def select_age(self):
        '''选择年龄'''
        logging.info("====start select age====")
        try:
            age_select = self.driver.find_element(*self.age_select)
        except NoSuchElementException:
            logging.info("can not find the age select")
            return False
        else:
            age_select.click()
            time.sleep(1)
            self.swipeDown(a1=500, b1=1100, a2=500, b2=850)
            try:
                ok = self.driver.find_element(*self.ok_button)
            except NoSuchElementException:
                logging.info("can not find ok button")
                return False
            else:
                ok.click()
                time.sleep(1)
                if age_select.get_attribute("text") == "Select your age":
                    logging.info("you still not select age")
                    return False
                else:
                    logging.info("you  select age:" + (age_select.get_attribute("text")))
                    return True

    def confirm_click(self):
        '''点击确认注册按钮'''
        logging.info("====click confirm====")
        try:
            confirm = self.driver.find_element(*self.confirm)
        except NoSuchElementException:
            logging.info("can not find confirm button")
            return False
        else:
            confirm.click()
            logging.info("confirm button clicked")
            return True


    def select_gender(self,gender):
        '''性别选择'''
        logging.info("====start select gender====")
        if gender == "Male":
            try:
                male = self.driver.find_element(*self.male)
            except NoSuchElementException:
                logging.info("can not find the male select")
                return False
            else:
                male.click()
                logging.info("gender set male")
                return True
        elif gender == "Female":
            try:
                female = self.driver.find_element(*self.female)
            except NoSuchElementException:
                logging.info("can not find the female select")
                return False
            else:
                female.click()
                logging.info("gender set female")
                return True
        elif gender == "Other":
            try:
                other = self.driver.find_element(*self.other)
            except NoSuchElementException:
                logging.info("can not find the other select")
                return False
            else:
                other.click()
                logging.info("gender set other")
                return True
        else:
            logging.info("can not found match gender")
            return False


if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = RegisterView(driver)
    l.go_Phone_login()
    l.phone_number_input( number="15511111117")
    l.verification_code_input("123456")
    time.sleep(2)
    l.first_header_edit()
    ran_name = ''.join(random.sample(string.ascii_letters + string.digits, 10))
    l.username_input(ran_name)
    # l.select_gender(gender="male")
    l.select_age()
    l.confirm_click()
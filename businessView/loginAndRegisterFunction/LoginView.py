from common.desired_caps import appium_desired
from businessView.homeFunction.HomeView import HomeView
from businessView.meFunction.meSetting.MeSettingsView import MeSettingsView
from common.common_fun import Common
from selenium.common.exceptions import NoSuchElementException
import logging,time,os
from selenium.webdriver.common.by import By

class LoginView(MeSettingsView,Common):

    fb_login_button = (By.ID,'com.test.voice.chat:id/btnFbLogin')
    snapchat_login_button = (By.ID,'com.test.voice.chat:id/btnSnapchatLogin')
    google_login_button = (By.ID,'com.test.voice.chat:id/tvGoogleLogin')
    twitter_login_button = (By.ID,'com.test.voice.chat:id/btnTwitterLogin')
    phone_login_button = (By.ID,'com.test.voice.chat:id/btnPhoneLogin')
    phone_number = (By.ID,'com.test.voice.chat:id/phone_number_edit')
    country_code = (By.ID,'com.test.voice.chat:id/country_code')
    select_country = (By.ID,'com.test.voice.chat:id/title')
    country_title = (By.ID,'com.test.voice.chat:id/country_title')
    country_code_tv = (By.ID,'com.test.voice.chat:id/country_code_tv')
    phone_login = (By.ID,'com.test.voice.chat:id/btnLogin')
    verification_code_tip = (By.ID,'com.test.voice.chat:id/phone_number_tv')
    verification_code = (By.CLASS_NAME, 'android.widget.EditText')
    toast_message = (By.XPATH,'//*[@resource-id="android:id/message"]')
    home_content = (By.ID,'com.test.voice.chat:id/home_content_recycler_view')

    def go_FB_login(self):
        '''FaceBook注册/登录'''
        try:
            self.driver.find_element(*self.fb_login_button)
        except NoSuchElementException:
            logging.info("====go to log out====")
            self.go_Me()
            self.go_Setting()
            self.log_Out()
            self.settings_twice_window(True)
            self.check_log_out_result()
            logging.info("====start Facebook login====")
            try:
                fb_login = self.driver.find_element(*self.fb_login_button)
            except NoSuchElementException:
                logging.info("can not find the FB login button")
                return False
            else:
                fb_login.click()
                logging.info("facebook login click")
                return True
        else:
            try:
                fb_login = self.driver.find_element(*self.fb_login_button)
            except NoSuchElementException:
                logging.info("can not find the FB login button")
                return False
            else:
                fb_login.click()
                logging.info("facebook login click")
                return True

    def go_snapchat_login(self):
        '''Instrgram注册/登录'''
        try:
            self.driver.find_element(*self.snapchat_login_button)
        except NoSuchElementException:
            logging.info("====go to log out====")
            self.go_Me()
            self.go_Setting()
            self.log_Out()
            self.settings_twice_window(True)
            self.check_log_out_result()
            logging.info("====start snapchat login====")
            try:
                INS_login = self.driver.find_element(*self.snapchat_login_button)
            except NoSuchElementException:
                logging.info("can not find the snapchat login button")
                return False
            else:
                INS_login.click()
                logging.info("snapchat login click")
                return True
        else:
            try:
                INS_login = self.driver.find_element(*self.snapchat_login_button)
            except NoSuchElementException:
                logging.info("can not find the snapchat login button")
                return False
            else:
                INS_login.click()
                logging.info("snapchat login click")
                return True

    def Go_twitter_login(self):
        '''twitter注册/登录'''
        try:
            self.driver.find_element(*self.twitter_login_button)
        except NoSuchElementException:
            logging.info("====go to log out====")
            self.go_Me()
            self.go_Setting()
            self.log_Out()
            self.settings_twice_window(True)
            self.check_log_out_result()
            logging.info("====start twitter login====")
            try:
                twitter_login = self.driver.find_element(*self.twitter_login_button)
            except NoSuchElementException:
                logging.info("can not find the Twitter login button")
                return False
            else:
                twitter_login.click()
                logging.info("twitter login click")
                return True
        else:
            try:
                twitter_login = self.driver.find_element(*self.twitter_login_button)
            except NoSuchElementException:
                logging.info("can not find the Twitter login button")
                return False
            else:
                twitter_login.click()
                logging.info("twitter login click")
                return True

    def go_Google_login(self):
        '''Google注册/登录'''
        try:
            self.driver.find_element(*self.google_login_button)
        except NoSuchElementException:
            logging.info("====go to log out====")
            self.go_Me()
            self.go_Setting()
            self.log_Out()
            self.settings_twice_window(True)
            self.check_log_out_result()
            logging.info("====start Google login====")
            try:
                Google_login = self.driver.find_element(*self.google_login_button)
            except NoSuchElementException:
                logging.info("can not find the google login button")
                return False
            else:
                Google_login.click()
                logging.info("google login click")
                return True
        else:
            try:
                Google_login = self.driver.find_element(*self.google_login_button)
            except NoSuchElementException:
                logging.info("can not find the google login button")
                return False
            else:
                Google_login.click()
                logging.info("google login click")
                return True

    def go_Phone_login(self):
        '''电话号码登录'''
        try:
            self.driver.find_element(*self.phone_login_button)
        except NoSuchElementException:
            logging.info("====go to log out====")
            self.go_Me()
            self.go_Setting()
            self.log_Out()
            self.settings_twice_window(True)
            self.check_log_out_result()
            logging.info("====start Phone number login====")
            try:
                Phone_login = self.driver.find_element(*self.phone_login_button)
            except NoSuchElementException:
                logging.info("can not find the phone login button")
                return False
            else:
                Phone_login.click()
                logging.info("phone login click")
                return True
        else:
            logging.info("====start Phone number login====")
            try:
                Phone_login = self.driver.find_element(*self.phone_login_button)
            except NoSuchElementException:
                logging.info("can not find the phone login button")
                return False
            else:
                Phone_login.click()
                logging.info("phone login click")
                return True

    def phone_country_select(self,goal_country):
        '''手机号码输入及国家选择'''
        logging.info("====choose country code====")
        try:
            country_code = self.driver.find_element(*self.country_code)
        except NoSuchElementException:
            logging.info("====can not find the country code====")
            return False
        else:
            country_code.click()
            if (self.driver.find_element(*self.select_country)).get_attribute('text') == 'Select Country':
                logging.info("you are in Select Country page")
            else:
                logging.info("Failed to go Select Country page")
                return False
            #自动查找选中目标国家号码
            for i in range(0,100):
                for j in range(0,13):
                    country = self.driver.find_elements(*self.country_title)[j]
                    country_name = (country).get_attribute("text")
                    if country_name == goal_country:
                        break
                if country_name == goal_country:
                    break
                self.swipeCountryDown()
            country.click()
            return True


    def phone_number_input(self,number):
        try:
            phone_number = self.driver.find_element(*self.phone_number)
        except NoSuchElementException:
            logging.info("can not find the number input")
            return False
        else:
            phone_number.send_keys(number)
            try:
                phone_login = self.driver.find_element(*self.phone_login)
            except NoSuchElementException:
                logging.info("can not find the login button")
                return False
            else:
                phone_login.click()
                try:
                    self.driver.find_element(*self.verification_code_tip)
                except NoSuchElementException:
                    logging.info("you are not in verifictaion code page")
                    return False
                else:
                    logging.info("you are in verification code page")
                    return True

    def verification_code_input(self,verification_code):
        '''手机号验证码输入'''
        code = list(verification_code)
        logging.info("Please input verification code："+verification_code)
        for i in range(0,6):
            verification = self.driver.find_elements(*self.verification_code)[i]
            verification.send_keys(code[i])
        time.sleep(2)
        return True

    def check_login(self):
        if self.go_Me() == True:
            logging.info('登录成功！')
            return True
        else:
            logging.info('登录失败！')
            return False

if __name__ == '__main__':
    yaml_address = "../../config/desired.yaml"
    driver = appium_desired(yaml_address)
    l = LoginView(driver)
    # l.go_FB_login()
    l.go_Phone_login()
    l.phone_number_input(number="15511111111")
    l.verification_code_input("123456")
    window = l.check_sign_window()
    if window == False:
        pass
    elif window == True:
        l.sign_get_gift()
    else:
        logging.info("something wrong")
    l.check_login()